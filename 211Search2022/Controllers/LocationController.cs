﻿using _211Models;
using _211Models.PostModel;
using _211Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;

namespace _211Search2022.Controllers
{
    public class LocationController : ApiController
    {
        private LocationServices locationServices;
        private const string Container = "images";
        private object rootobject;

       

        [System.Web.Http.HttpPost]
        [System.Web.Http.Route("LocationHierarchy")]
        public IHttpActionResult LocationHierarchy(LocationHierarchyModel locationHierarchyModel)
        {
            locationServices = new LocationServices();
            ServiceResponse serviceResponse = new ServiceResponse();
            serviceResponse = locationServices.LocationHierarchy(locationHierarchyModel);
            return Content<ServiceResponse>(HttpStatusCode.OK, serviceResponse);
        }
      
    }
}