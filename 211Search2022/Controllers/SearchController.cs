﻿using _211Models;
using _211Models.PostModel;
using _211Services;
using Microsoft.Xrm.Sdk;
using System;
using System.Net;
using System.Net.Http;
using System.Web.Http;


namespace _211Search2022.Controllers
{
    public class SearchController : ApiController
    {
        private SearchServices searchServices;
        private const string Container = "images";
        private object rootobject;

        [System.Web.Http.HttpPost]
        [System.Web.Http.Route("GetDataFromTaxonomyTerms")]

        public IHttpActionResult GetDataFromTaxonomyTerms(HighlightedTermPostModel highlightedTermPostModel)
        {
            searchServices = new SearchServices();
            ServiceResponse serviceResponse = new ServiceResponse();
            serviceResponse = searchServices.GetDataFromTaxonomyTerms(highlightedTermPostModel);
            return Content<ServiceResponse>(HttpStatusCode.OK, serviceResponse);
        }
        [System.Web.Http.HttpPost]
        [System.Web.Http.Route("GetDataFromCategories")]

        public IHttpActionResult GetDataFromCategories(ResultResponseFromCategories resultResponseFromCategories)
        {
            searchServices = new SearchServices();
            ServiceResponse serviceResponse = new ServiceResponse();
            serviceResponse = searchServices.GetDataFromCategories(resultResponseFromCategories);
            return Content<ServiceResponse>(HttpStatusCode.OK, serviceResponse);
        }


        [System.Web.Http.HttpPost]
        [System.Web.Http.Route("GlobalSearch")]

        public IHttpActionResult GlobalSearch(GlobalSearchPostModel globalSearchPostModel)
        {
            searchServices = new SearchServices();
            ServiceResponse serviceResponse = new ServiceResponse();
            serviceResponse = searchServices.GlobalSearch(globalSearchPostModel);
            return Content<ServiceResponse>(HttpStatusCode.OK, serviceResponse);
        }

        [System.Web.Http.HttpPost]
        [System.Web.Http.Route("GetSuggetionAll")]

        public IHttpActionResult GetSuggetionAll(SuggestionModel suggetionModel)
        {
            searchServices = new SearchServices();
            ServiceResponse serviceResponse = new ServiceResponse();
            serviceResponse = searchServices.GetSuggetionAll(suggetionModel);
            return Content<ServiceResponse>(HttpStatusCode.OK, serviceResponse);
        }

        [System.Web.Http.HttpPost]
        [System.Web.Http.Route("GetSuggetion")]

        public IHttpActionResult GetSuggetion(SuggestionModel suggetionModel)
        {
            searchServices = new SearchServices();
            ServiceResponse serviceResponse = new ServiceResponse();
            serviceResponse = searchServices.GetSuggetion(suggetionModel);
            return Content<ServiceResponse>(HttpStatusCode.OK, serviceResponse);
        }

        [System.Web.Http.HttpPost]
        [System.Web.Http.Route("AllFieldLogicalName")]

        public IHttpActionResult AllFieldLogicalName()
        {
            searchServices = new SearchServices();
            ServiceResponse serviceResponse = new ServiceResponse();
            serviceResponse = searchServices.AllFieldLogicalName();
            return Content<ServiceResponse>(HttpStatusCode.OK, serviceResponse);
        }

        [System.Web.Http.HttpPost]
        [System.Web.Http.Route("GetDataByResourceId")]

        public IHttpActionResult GetDataByResourceId(DataResourceId dataResourceId)
        {
            searchServices = new SearchServices();
            ServiceResponse serviceResponse = new ServiceResponse();
            serviceResponse = searchServices.GetDataByResourceId(dataResourceId);
            return Content<ServiceResponse>(HttpStatusCode.OK, serviceResponse);
        }
        [System.Web.Http.HttpPost]
        [System.Web.Http.Route("HighlightedTerms")]

        public IHttpActionResult HighlightedTerms()
        {
            searchServices = new SearchServices();
            ServiceResponse serviceResponse = new ServiceResponse();
            serviceResponse = searchServices.HighlightedTerms();
            return Content<ServiceResponse>(HttpStatusCode.OK, serviceResponse);
        }
        [System.Web.Http.HttpPost]
        [System.Web.Http.Route("AddSavedSearch")]

        public IHttpActionResult AddSavedSearch(PredefineSearchModel predefineSearchModel)
        {
            searchServices = new SearchServices();
            ServiceResponse serviceResponse = new ServiceResponse();
            serviceResponse = searchServices.AddSavedSearch(predefineSearchModel);
            return Content<ServiceResponse>(HttpStatusCode.OK, serviceResponse);
        }
        [System.Web.Http.HttpPost]
        [System.Web.Http.Route("GetSavedSearch")]

        public IHttpActionResult GetSavedSearch(PredefineSearchModel predefineSearchModel)
        {
            searchServices = new SearchServices();
            ServiceResponse serviceResponse = new ServiceResponse();
            serviceResponse = searchServices.GetSavedSearch(predefineSearchModel);
            return Content<ServiceResponse>(HttpStatusCode.OK, serviceResponse);
        }

        [System.Web.Http.HttpPost]
        [System.Web.Http.Route("UpdateEntity")]
        public IHttpActionResult UpdateEntity(PredefineSearchModel predefineSearchModel)
        {
            searchServices = new SearchServices();
            ServiceResponse serviceResponse = new ServiceResponse();
            serviceResponse = searchServices.UpdateEntity(predefineSearchModel);
            return Content<ServiceResponse>(HttpStatusCode.OK, serviceResponse);
        }
        [System.Web.Http.HttpPost]
        [System.Web.Http.Route("GetagentId")]

        public IHttpActionResult GetagentId(CaseModel caseModel)
        {
            searchServices = new SearchServices();
            ServiceResponse serviceResponse = new ServiceResponse();
            serviceResponse = searchServices.GetagentId(caseModel);
            return Content<ServiceResponse>(HttpStatusCode.OK, serviceResponse);
        }

        [System.Web.Http.HttpPost]
        [System.Web.Http.Route("DatabaseReferrals")]

        public IHttpActionResult DatabaseReferrals(DB_ReferralsPostModel dB_Referrals)
        {
            if(ModelState.IsValid)
            {
                searchServices = new SearchServices();
                ServiceResponse serviceResponse = new ServiceResponse();
                serviceResponse = searchServices.DatabaseReferrals(dB_Referrals);
                return Content<ServiceResponse>(HttpStatusCode.OK, serviceResponse);
            }
            else
            {
                return BadRequest(ModelState);
            }
            
        }

        [System.Web.Http.HttpPost]
        [System.Web.Http.Route("TaxonomyTree")]

        public IHttpActionResult TaxonomyTree(TaxonomyTreePostModel taxonomyTreePostModel)
        {
            searchServices = new SearchServices();
            ServiceResponse serviceResponse = new ServiceResponse();
            serviceResponse = searchServices.TaxonomyTree(taxonomyTreePostModel);
            return Content<ServiceResponse>(HttpStatusCode.OK, serviceResponse);
        }

        [System.Web.Http.HttpPost]
        [System.Web.Http.Route("CategoriesTree")]

        public IHttpActionResult CategoriesTree(CategoriesTreeRequestModel categoriesTreeRequestModel)
        {
            searchServices = new SearchServices();
            ServiceResponse serviceResponse = new ServiceResponse();
            serviceResponse = searchServices.CategoriesTree(categoriesTreeRequestModel);
            return Content<ServiceResponse>(HttpStatusCode.OK, serviceResponse);
        }

        [System.Web.Http.HttpPost]
        [System.Web.Http.Route("MakeReferal")]

        public IHttpActionResult MakeReferal(MakeReferalPostModel makeReferalPostModel)
        {
            
            searchServices = new SearchServices();
            ServiceResponse serviceResponse = new ServiceResponse();
            serviceResponse = searchServices.MakeReferal(makeReferalPostModel);
            return Content<ServiceResponse>(HttpStatusCode.OK, serviceResponse);
        }

        [System.Web.Http.HttpPost]
        [System.Web.Http.Route("GetOutofDBDATA")]

        public IHttpActionResult GetOutofDBDATA(PaginModel pagingModel)
        {
            searchServices = new SearchServices();
            ServiceResponse serviceResponse = new ServiceResponse();
            serviceResponse = searchServices.GetOutofDBDATA(pagingModel);
            return Content<ServiceResponse>(HttpStatusCode.OK, serviceResponse);
        }

        [System.Web.Http.HttpPost]
        [System.Web.Http.Route("SubmitIssue")]

        public IHttpActionResult SubmitIssue(verificationIssuePostModel verificationIssue)
        {
            searchServices = new SearchServices();
            ServiceResponse serviceResponse = new ServiceResponse();
            serviceResponse = searchServices.SubmitIssue(verificationIssue);

            return Content<ServiceResponse>(HttpStatusCode.OK, serviceResponse);
        }

        [System.Web.Http.HttpPost]
        [System.Web.Http.Route("AddUpdatePossibleSearch")]

        public IHttpActionResult AddUpdatePossibleSearch(Addupdatetermpostmodel addupdatetermpostmodel)
        {
            searchServices = new SearchServices();
            ServiceResponse serviceResponse = new ServiceResponse();
            serviceResponse = searchServices.AddUpdatePossibleSearch(addupdatetermpostmodel);
            return Content<ServiceResponse>(HttpStatusCode.OK, serviceResponse);
        }

        [System.Web.Http.HttpPost]
        [System.Web.Http.Route("OTBMakeRefferal")]

        public IHttpActionResult OTBMakeRefferal(OTBMakeRefferalRequest oTBMakeRefferalRequest)
        {
            searchServices = new SearchServices();
            ServiceResponse serviceResponse = new ServiceResponse();
            serviceResponse = searchServices.OTBMakeRefferal(oTBMakeRefferalRequest);
            return Content<ServiceResponse>(HttpStatusCode.OK, serviceResponse);
        }

        [System.Web.Http.HttpPost]
        [System.Web.Http.Route("GetResourcesFromCRM")]

        public IHttpActionResult GetResourcesFromCRM(Addupdatetermpostmodel addupdatetermpostmodel)
        {
            searchServices = new SearchServices();
            ServiceResponse serviceResponse = new ServiceResponse();
            serviceResponse = searchServices.GetResourcesFromCRM(addupdatetermpostmodel);
            return Content<ServiceResponse>(HttpStatusCode.OK, serviceResponse);
        }
        
        [System.Web.Http.HttpPost]
        [System.Web.Http.Route("GeographicfilterResults")]

        public IHttpActionResult GeographicfilterResults(GeoFiltersPostModel geoFiltersPostModel )
        {
            searchServices = new SearchServices();
            ServiceResponse serviceResponse = new ServiceResponse();
            serviceResponse = searchServices.GeographicfilterResults(geoFiltersPostModel);
            return Content<ServiceResponse>(HttpStatusCode.OK, serviceResponse);
        }
        [System.Web.Http.HttpPost]
        [System.Web.Http.Route("TaxonomyTermSuggestion")]

        public IHttpActionResult TaxonomyTermSuggestion(SuggestionModel suggetionModel)
        {
            searchServices = new SearchServices();
            ServiceResponse serviceResponse = new ServiceResponse();
            serviceResponse = searchServices.TaxonomyTermSuggestion(suggetionModel);
            return Content<ServiceResponse>(HttpStatusCode.OK, serviceResponse);
        }

        [System.Web.Http.HttpPost]
        [System.Web.Http.Route("AnnotationNotes")]

        public IHttpActionResult AnnotationNotes(AnnotationCase annotationCase)
        {
            searchServices = new SearchServices();
            ServiceResponse serviceResponse = new ServiceResponse();
            serviceResponse = searchServices.AnnotationNotes(annotationCase);
            return Content<ServiceResponse>(HttpStatusCode.OK, serviceResponse);
        }
        [System.Web.Http.HttpPost]
        [System.Web.Http.Route("TaxonomyRedirect")]

        public IHttpActionResult TaxonomyRedirect(TaxonomyDirectionpostModel taxonomyDirectionpost)
        {
            searchServices = new SearchServices();
            ServiceResponse serviceResponse = new ServiceResponse();
            serviceResponse = searchServices.TaxonomyRedirect(taxonomyDirectionpost);
            return Content<ServiceResponse>(HttpStatusCode.OK, serviceResponse);
        }
        [System.Web.Http.HttpPost]
        [System.Web.Http.Route("GetAnnotation")]

        public IHttpActionResult GetAnnotation(AnnotationCase annotationCase)
        {
            searchServices = new SearchServices();
            ServiceResponse serviceResponse = new ServiceResponse();
            serviceResponse = searchServices.GetAnnotation(annotationCase);
            return Content<ServiceResponse>(HttpStatusCode.OK, serviceResponse);
        }

        [System.Web.Http.HttpPost]
        [System.Web.Http.Route("UpdatereferralList")]

        public IHttpActionResult UpdatereferralList(ReferralPostModel referralPostModel)
        {
            searchServices = new SearchServices();
            ServiceResponse serviceResponse = new ServiceResponse();
            serviceResponse = searchServices.UpdatereferralList(referralPostModel);
            return Content<ServiceResponse>(HttpStatusCode.OK, serviceResponse);
        }

        [System.Web.Http.HttpPost]
        [System.Web.Http.Route("UpdateNeedList")]

        public IHttpActionResult UpdateNeedList(UpdatePostModel updatePostModel)
        {
            searchServices = new SearchServices();
            ServiceResponse serviceResponse = new ServiceResponse();
            serviceResponse = searchServices.UpdateNeedList(updatePostModel);
            return Content<ServiceResponse>(HttpStatusCode.OK, serviceResponse);
        }

        [System.Web.Http.HttpPost]
        [System.Web.Http.Route("TaxonomyTermSuggestionForReferral")]

        public IHttpActionResult TaxonomyTermSuggestionForReferral(TaxonomytermsearchsuggestionModel taxonomytermsearchsuggestionModel)
        {
            searchServices = new SearchServices();
            ServiceResponse serviceResponse = new ServiceResponse();
            serviceResponse = searchServices.TaxonomyTermSuggestionForReferral(taxonomytermsearchsuggestionModel);
            return Content<ServiceResponse>(HttpStatusCode.OK, serviceResponse);
        }

        [System.Web.Http.HttpPost]
        [System.Web.Http.Route("GetcategoriesdatabyResourceId")]

        public IHttpActionResult GetcategoriesdatabyResourceId(CategorizedDataViewModel dataResourceId)
        {
            searchServices = new SearchServices();
            ServiceResponse serviceResponse = new ServiceResponse();
            serviceResponse = searchServices.GetcategoriesdatabyResourceId(dataResourceId);
            return Content<ServiceResponse>(HttpStatusCode.OK, serviceResponse);
        }

        [System.Web.Http.HttpPost]
        [System.Web.Http.Route("GetCurrentEmail")]

        public IHttpActionResult GetCurrentEmail(GetEmailRequest getEmailRequest)
        {
            searchServices = new SearchServices();
            ServiceResponse serviceResponse = new ServiceResponse();
            serviceResponse = searchServices.GetEmailId(getEmailRequest);
            return Content<ServiceResponse>(HttpStatusCode.OK, serviceResponse);
        }

        [System.Web.Http.HttpPost]
        [System.Web.Http.Route("CreateUsedTerms")]

        public IHttpActionResult CreateUsedTerms(CreateUsedTermsRequest createUsedTermsRequest)
        {
            searchServices = new SearchServices();
            ServiceResponse serviceResponse = new ServiceResponse();
            serviceResponse = searchServices.CreateUsedTerms(createUsedTermsRequest);
            return Content<ServiceResponse>(HttpStatusCode.OK, serviceResponse);
        }

        [System.Web.Http.HttpPost]
        [System.Web.Http.Route("GetUsedTerms")]

        public IHttpActionResult GetUsedTerms(CaseModel caseModel)
        {
            searchServices = new SearchServices();
            ServiceResponse serviceResponse = new ServiceResponse();
            serviceResponse = searchServices.GetUsedTerms(caseModel);
            return Content<ServiceResponse>(HttpStatusCode.OK, serviceResponse);
        }

        [System.Web.Http.HttpPost]
        [System.Web.Http.Route("SendMultipleReferralByEmail")]

        public IHttpActionResult SendMultipleReferralByEmail(MailRequestModel mailRequestModel)
        {
            searchServices = new SearchServices();
            ServiceResponse serviceResponse = new ServiceResponse();
            serviceResponse = searchServices.sendMultipleReferralByEmail(mailRequestModel);
            return Content<ServiceResponse>(HttpStatusCode.OK, serviceResponse);
        }

        [System.Web.Http.HttpPost]
        [System.Web.Http.Route("DownloadReferral")]

        public IHttpActionResult DownloadReferral(ReferralModel referralModel)
        {
            searchServices = new SearchServices();
            ServiceResponse serviceResponse = new ServiceResponse();
            serviceResponse = searchServices.downloadReferral(referralModel);
            return Content<ServiceResponse>(HttpStatusCode.OK, serviceResponse);
        }

        [System.Web.Http.HttpPost]
        [System.Web.Http.Route("RemoveRecord")]

        public IHttpActionResult RemoveRecord(RemoveRecordRequest removeRecordRequest)
        {
            searchServices = new SearchServices();
            ServiceResponse serviceResponse = new ServiceResponse();
            serviceResponse = searchServices.RemoveRecord(removeRecordRequest);
            return Content<ServiceResponse>(HttpStatusCode.OK, serviceResponse);
        }

        [System.Web.Http.HttpPost]
        [System.Web.Http.Route("Removereferralforusedterm")]

        public IHttpActionResult Removereferralforusedterm(Removereqeuestforreferral removeRecordRequest)
        {
            searchServices = new SearchServices();
            ServiceResponse serviceResponse = new ServiceResponse();
            serviceResponse = searchServices.Removereferralforusedterm(removeRecordRequest);
            return Content<ServiceResponse>(HttpStatusCode.OK, serviceResponse);
        }

        [System.Web.Http.HttpPost]
        [System.Web.Http.Route("CreateusedTermsMultiple")]

        public IHttpActionResult CreateusedTermsMultiple(CreateUsedTermsMultipleRequest createUsedTermsMultiple)
        {
            searchServices = new SearchServices();
            ServiceResponse serviceResponse = new ServiceResponse();
            serviceResponse = searchServices.CreateusedTermsMultiple(createUsedTermsMultiple);
            return Content<ServiceResponse>(HttpStatusCode.OK, serviceResponse);
        }
        [System.Web.Http.HttpPost]
        [System.Web.Http.Route("GetProximityDistance")]

        public IHttpActionResult GetProximityDistance()
        {
            searchServices = new SearchServices();
            ServiceResponse serviceResponse = new ServiceResponse();
            serviceResponse = searchServices.GetProximityDistance();
            return Content<ServiceResponse>(HttpStatusCode.OK, serviceResponse);
        }

        [System.Web.Http.HttpPost]
        [System.Web.Http.Route("GetLocationDetailsOfCase")]

        public IHttpActionResult GetLocationDetailsOfCase(CasePostModel casePostModel)
        {
            searchServices = new SearchServices();
            ServiceResponse serviceResponse = new ServiceResponse();
            serviceResponse = searchServices.GetLocationDetailsOfCase(casePostModel);
            return Content<ServiceResponse>(HttpStatusCode.OK, serviceResponse);
        }
    }
}