﻿using _211Services;
using System;
using System.Collections.Generic;
using System.Web.Http;
using System.Web.Mvc;
using Microsoft.Owin.Security;
using Microsoft.Owin.Security.OAuth;
using _211Models.PostModel;
using _211Models;
using System.Security.Claims;
using System.Net;

namespace _211Search2022.Controllers
{
    public class UserSecurityController : ApiController
    {
        private UserSecurityServices userSecurityServices;
        private const string Container = "images";
        private object rootobject;

        [System.Web.Http.Route("Login")]
        public IHttpActionResult Login(LoginModel loginModel)
        {
            userSecurityServices = new UserSecurityServices();
            ServiceResponse serviceResponse = new ServiceResponse();
            serviceResponse = userSecurityServices.Login(loginModel);
            if (serviceResponse != null && serviceResponse.ResponseType == "1")
            {
                // Initialization.  
                var claims = new List<Claim>();
                //var userInfo = user.FirstOrDefault();

                // Setting  
                claims.Add(new Claim(System.Security.Claims.ClaimTypes.Name, loginModel.UserName + loginModel.Password + loginModel.StaffNumberId));

                // Setting Claim Identities for OAUTH 2 protocol.  
                ClaimsIdentity oAuthClaimIdentity = new ClaimsIdentity(claims, OAuthDefaults.AuthenticationType);
                //ClaimsIdentity cookiesClaimIdentity = new ClaimsIdentity(claims, CookieAuthenticationDefaults.AuthenticationType);

                // Setting user authentication. 
                IDictionary<string, string> data = new Dictionary<string, string> { { "UserName_Password", loginModel.UserName + loginModel.Password + loginModel.StaffNumberId } };

                //string OauthSessionHoursFaculty = System.Configuration.ConfigurationManager.AppSettings["OauthSessionHoursFaculty"];

                AuthenticationProperties properties = new AuthenticationProperties(data)
                {
                    ExpiresUtc = DateTime.UtcNow.AddMinutes(300),
                    IssuedUtc = DateTime.UtcNow
                };
                AuthenticationTicket ticket = new AuthenticationTicket(oAuthClaimIdentity, properties);
                Startup.OAuthOptions.AllowInsecureHttp = false;
                string tokenKey = Startup.OAuthOptions.AccessTokenFormat.Protect(ticket);

                serviceResponse.Token = tokenKey;
            }
            return Content<ServiceResponse>(HttpStatusCode.OK, serviceResponse);
        }

     
    }
}