﻿using _211Models;
using _211Models.ViewModel;
using _211Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace _211Search2022.Controllers
{
    public class DownloadDataByResourceController : ApiController
    {

        private DownloadDatabyResourceId downloadDatabyResourceId;
        //private GetResouceDataById getResouceDataById;


        [HttpPost]
        [Route("DownloadDataByResource")]
        //  public IHttpActionResult DownloadDatabyResourc(DownloadDataResourceModel downloadDataResourceModel)
        public IHttpActionResult DownloadDatabyResourc(DownloadCsvModel downloadDataResourceModel)
        {
            downloadDatabyResourceId = new DownloadDatabyResourceId();
            ServiceResponse serviceResponse = new ServiceResponse();
            //  serviceResponse = downloadDatabyResourceId.GetDataByResouceId(downloadDataResourceModel);
            serviceResponse = downloadDatabyResourceId.ExportResource(downloadDataResourceModel);
            return Content<ServiceResponse>(HttpStatusCode.OK, serviceResponse);
        }

        
    }
}
