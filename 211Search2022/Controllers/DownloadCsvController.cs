﻿using _211Models;
using _211Models.PostModel;
using _211Models.ViewModel;
using _211Services;
using System.Net;
using System.Web.Http;

namespace _211Search2022.Controllers
{
    public class DownloadCsvController : ApiController
    {
        private DownloadCsvService downloadCsvService;

        [HttpPost]
        [Route("ExportData")]
        public IHttpActionResult ExportData(DownloadCsvModel downloadCsvModel)
        {
            downloadCsvService = new DownloadCsvService();
            ServiceResponse serviceResponse = new ServiceResponse();
            serviceResponse = downloadCsvService.ExportData(downloadCsvModel);
            return Content<ServiceResponse>(HttpStatusCode.OK, serviceResponse);
        }
    }
}
