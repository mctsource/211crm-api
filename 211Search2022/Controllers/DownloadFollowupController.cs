﻿using _211Models;
using _211Models.ViewModel;
using _211Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace _211Search2022.Controllers
{
    public class DownloadFollowupController : ApiController
    {
        FollowUpRepostService followUpRepostService;
        [HttpPost]
        [Route("DownloadFollowupReport")]
        //  public IHttpActionResult DownloadDatabyResourc(DownloadDataResourceModel downloadDataResourceModel)
        public IHttpActionResult DownloadDatabyResourc(DownloadCsvModel downloadDataResourceModel)
        {
            followUpRepostService = new FollowUpRepostService();
            ServiceResponse serviceResponse = new ServiceResponse();
            serviceResponse = followUpRepostService.FollowupReport(downloadDataResourceModel);
            return Content<ServiceResponse>(HttpStatusCode.OK, serviceResponse);
        }
    }
}
