﻿using _211Models;
using _211Models.ViewModel;
using _211Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace _211Search2022.Controllers
{
    public class DownloadNeedController : ApiController
    {
        NeedsReportService needsReportService;
        [HttpPost]
        [Route("DownloadNeedReport")]
        //  public IHttpActionResult DownloadDatabyResourc(DownloadDataResourceModel downloadDataResourceModel)
        public IHttpActionResult DownloadDatabyResourc(DownloadCsvModel downloadDataResourceModel)
        {
            needsReportService = new NeedsReportService();
            ServiceResponse serviceResponse = new ServiceResponse();
            serviceResponse = needsReportService.NeedsReport(downloadDataResourceModel);
            return Content<ServiceResponse>(HttpStatusCode.OK, serviceResponse);
        }
    }
}
