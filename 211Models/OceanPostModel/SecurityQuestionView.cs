﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _211Models.OceanPostModel
{
    public class SecurityQuestionView
    {
        public string SecurityQuestionId { get; set; }
        public string SecurityQuestion { get; set; }
        public string SecurityQuestionAns { get; set; }
    }
}
