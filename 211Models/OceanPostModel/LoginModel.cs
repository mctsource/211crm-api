﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _211Models.OceanPostModel
{
    public class LoginModel : BaseModel
    {
        public string UserName { get; set; }
        public string Password { get; set; }
        public string Otp { get; set; }
        public string ContactId { get; set; }
        public string MobileNo { get; set; }
        public string StaffNumberId { get; set; }
      
        public string DeviceToken { get; set; }
        public IList<SecurityQuestionView> SecurityQuestionView { get; set; }
    }
}
