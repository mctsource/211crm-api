﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _211Models
{
    public class PredefineSearchModel
    {
        public string GlobalSearch { get; set; }
        public string FieldName { get; set; }
        public string Agency { get; set; }
        public string Site { get; set; }
        public string Active { get; set; }
        public string LinkedOnly { get; set; }
        public string Province { get; set; }
        public string SearchType { get; set; }
        public string City { get; set; }
        public string SavedSearchName { get; set; }
        public string TaxonomyCondition1 { get; set; }
        public string Program { get; set; }
        public string ProgramAtSite { get; set; }
        public string Inactive { get; set; }
        public string Country { get; set; }
        public string County { get; set; }
        public string PostalCode { get; set; }
        public string TaxonomyConditionName { get; set; }
        public string TaxonomyCondition2 { get; set; }
        public string Agent { get; set; }
        public string searchId { get; set; }
        public string GeoFilter { get; set; }
        public string taxonomyField { get; set; }
       
    }
}
