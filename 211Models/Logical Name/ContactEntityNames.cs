﻿using Microsoft.Xrm.Sdk;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _211Models.LogicalName
{
    public class ContactEntityNames
    {
        public const string Self = "contact";
        public const string Id = "contactid";

        public static readonly string EmailAddress = "emailaddress1";
    }
}
