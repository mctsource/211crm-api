﻿using Microsoft.Xrm.Sdk;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _211Models.LogicalName
{
    public class MyResourcesEntityNames
    {
        public const string Self = "ccon_myresources";
        public const string Id = "ccon_myresourcesid";
        public static readonly string Needs = "ccon_needs";
        public static readonly string Name = "ccon_name";
        public static readonly string Case = "ccon_case";
        public static readonly string ReferralsMeetingned = "ccon_referralsmeetingned";
        public static readonly string ResourceHistoryNumber = "ccon_resourcehistorynumber";

        public static class StateCode
        {
            public static readonly string self = "statecode";
            public static readonly OptionSetValue Active = new OptionSetValue(0);
            public static readonly OptionSetValue InActive = new OptionSetValue(1);
        }
        public static class StatusCode
        {
            public static readonly string self = "statuscode";
            public static readonly OptionSetValue Active = new OptionSetValue(1);
            public static readonly OptionSetValue InActive = new OptionSetValue(2);
        }

        //public static class NeedStatus
        //{
        //    public static readonly string self = "ccon_needsstatus";
        //    public static readonly OptionSetValue Needwasmet = new OptionSetValue(757130000);
        //    public static readonly OptionSetValue FullwaitingList = new OptionSetValue(757130001);
        //    public static readonly OptionSetValue IneligibleForService = new OptionSetValue(757130002);
        //    public static readonly OptionSetValue NoResourceFoundToMeet = new OptionSetValue(757130003);
        //    public static readonly OptionSetValue AgencyProgramResourcesDepleted = new OptionSetValue(757130004);
        //    public static readonly OptionSetValue InquirerRefusedReferral = new OptionSetValue(757130005);
        //    public static readonly OptionSetValue InquirerUnableConnectAgencyProgram = new OptionSetValue(757130006);
        //    public static readonly OptionSetValue HourAgencyProgramNotMeetsNeedsInquirer = new OptionSetValue(757130007);
        //    public static readonly OptionSetValue CannotAffordService = new OptionSetValue(757130008);
        //    public static readonly OptionSetValue InquirerTransportation = new OptionSetValue(757130009);
        //    public static readonly OptionSetValue LanguageBarrier = new OptionSetValue(757130010);
        //}

        public static class Priority
        {
            public static readonly string self = "ccon_prioritytoneed";
            public static readonly OptionSetValue One = new OptionSetValue(757130000);
            public static readonly OptionSetValue Two = new OptionSetValue(757130001);
            public static readonly OptionSetValue Three = new OptionSetValue(757130002);
            public static readonly OptionSetValue Four = new OptionSetValue(757130003);
            public static readonly OptionSetValue Five = new OptionSetValue(757130004);
        }
    }
}
