﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _211Models.LogicalName
{
    public class CentralResourceEntityName
    {
        public const string Self = "ccon_centralresources";
        public const string Id = "ccon_centralresourcesid";

        public const string categories = "ccon_categories";
        public const string publicname = "ccon_publicname";
        public const string websiteaddress = "ccon_websiteaddress";
        public const string alternatename = "ccon_alternatename";
        public const string resourceagencynum = "ccon_resourceagencynum";
        public const string taxonomylevelname = "ccon_taxonomylevelname";
        public const string agencydescription = "ccon_agencydescription";
        public const string physicaladdress1 = "ccon_physicaladdress1";
        public const string physicaladdress2 = "ccon_physicaladdress2";
        public const string phonenumberbusinessline = "ccon_phonenumberbusinessline";
        public const string applicationprocess = "ccon_applicationprocess";
        public const string eligibility = "ccon_eligibility";
        public const string hoursofoperation = "ccon_hoursofoperation";
        public const string feestructuresource = "ccon_feestructuresource";
        public const string phonefax = "ccon_phonefax";
        public const string agencystatus = "ccon_agencystatus";
        public const string highlightedresource = "ccon_highlightedresource";
        public const string searchhint = "ccon_searchhints";
        public const string preferredprovider = "ccon_preferredprovider";
       
    }
}
