﻿using Microsoft.Xrm.Sdk;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _211Models.LogicalName
{
    public class NeedEntityNames
    {
        public const string Self = "ccon_need";
        public const string Id = "ccon_needid";
        public static readonly string Needs = "ccon_name";
        public static readonly string Case = "ccon_case"; 
        public static readonly string Resourcehistory = "ccon_resourcehistory";
        public static class StateCode
        {
            public static readonly string self = "statecode";
            public static readonly OptionSetValue Active = new OptionSetValue(0);
            public static readonly OptionSetValue InActive = new OptionSetValue(1);
        }
        public static class StatusCode
        {
            public static readonly string self = "statuscode";
            public static readonly OptionSetValue Active = new OptionSetValue(1);
            public static readonly OptionSetValue InActive = new OptionSetValue(2);
        }
    }
}
