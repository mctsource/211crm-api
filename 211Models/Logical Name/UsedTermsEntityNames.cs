﻿using Microsoft.Xrm.Sdk;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _211Models.LogicalName
{
    public class UsedTermsEntityNames
    {
        public const string Self = "ccon_termused";
        public const string Id = "ccon_termusedid";

        public static readonly string Case = "ccon_caselookup";
        public static readonly string ResourceAgencyNumber = "ccon_resourceagencynumber";
        public static readonly string TaxonomyTerms = "ccon_taxonomyterms";
        public static readonly string StateCode = "statecode"; 
        public static readonly string StatusCode = "statuscode";
    }
}
