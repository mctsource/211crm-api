﻿using Microsoft.Xrm.Sdk;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _211Models.LogicalName
{
    public class CaseEntityNames
    {
        public const string Self = "incident";
        public const string Id = "incidentid";
        //new_country  new_province  new_county  new_city  new_postalcode
        public const string country = "new_country";
        public const string province = "new_province";
        public const string county = "new_county";
        public const string city = "new_city";
        public const string postalcode = "new_postalcode";

        public static readonly string contact = "ccon_customername";
    }
}
