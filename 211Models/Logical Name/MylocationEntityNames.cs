﻿using _211Models.Properties;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _211Models.LogicalName
{
    public class MylocationEntityNames
    {
        public const string Self = "ccon_mylocation";
        public const string Id = "ccon_mylocationid";

        public const string LocationName = "ccon_name";
        public const string LocationType = "ccon_type";
        public const string ParentLocation = "ccon_parentlocation";
        public const string ProvinceCode = "ccon_provincecode";
        public const string categories = "ccon_categories";

        //var attributeRequest = new RetrieveAttributeRequest
        //{
        //    EntityLogicalName = Self,
        //    LogicalName = LocationType,
        //    RetrieveAsIfPublished = true
        //};

        //var attributeResponse = (RetrieveAttributeResponse)crmSvx.Execute(attributeRequest);
        //var attributeMetadata = (EnumAttributeMetadata)attributeResponse.AttributeMetadata;

        //var optionList = (from o in attributeMetadata.OptionSet.Options
        //                  select new { id = o.Value, name = o.Label.UserLocalizedLabel.Label }).ToList();
        //public IList<Optionset> LocationTypeList = (from o in attributeMetadata.OptionSet.Options
        //                          select new Optionset
        //                          { id = Convert.ToInt32(o.Value), name = o.Label.UserLocalizedLabel.Label }).ToList();


    }
}
