﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _211Models.LogicalName
{
    public class CategoryTreeEntityNames
    {
        public const string Self = "ccon_categorytree";
        public const string Id = "ccon_categorytreeid";

        public const string parentcategoryname = "ccon_parentcategoryid1";
        public const string categoryname = "ccon_name";
        public const string parentcategorycode = "ccon_parentcode";
        public const string categorycode = "ccon_childcode";
    }
}
