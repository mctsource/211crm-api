﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _211Models
{
    public class ServiceResponse
    {
        public string ResponseType { get; set; }
        public string ResponseMessge { get; set; }
        public object Data { get; set; }
        public string RedirectUrl { get; set; }
        public string Token { get; set; }
        public object PagingData { get; set; }
        public string DataCount { get; set; }
        public string APIError { get; set; }
        public object Datacache { get; set; }
        public string TaxonomyLevelName{get;set;}
        
    }
}
