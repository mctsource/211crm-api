﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _211Models.PostModel
{
    public class DB_ReferralsPostModel
    {
        [Required(ErrorMessage = "Title is required")]
        public string AgnecyName { get; set; }
        public string ServiceName { get; set; }
        public string AgencyWebsite { get; set; }
        public string WebsiteSearched { get; set; }
        public string PhoneNumber { get; set; }
        [RegularExpression(@"^([\w\.\-]+)@([\w\-]+)((\.(\w){2,3})+)$", ErrorMessage = "Enter Valid Email Address")]
        public string EmailAdress { get; set; }
        public string Adressline1 { get; set; }
        public string AdressLine2 { get; set; }
        public string City { get; set; }
        public string Province { get; set; }
        public string Country { get; set; }
        public string PostalCode { get; set; }
        public string OtherNotes { get; set; }
        public bool Isreferral { get; set; }
    }
}
