﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _211Models.PostModel
{
    public class MailRequestModel
    {
        public string CustomerEmail { get; set; }
        public string ResourceAgencyNumber { get; set; }
        public string CaseId { get; set; }
    }

    public class ReferralModel
    {
        public string ResourceAgencyNumber { get; set; }
    }
}
