﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _211Models.ViewModel
{
    public class DownloadCsvModel
    {
        public string start_date { get; set; }
        public string end_date { get; set; }
    }
}
