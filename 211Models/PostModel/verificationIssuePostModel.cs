﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _211Models.PostModel
{
    public class verificationIssuePostModel
    {
        public string Issue { get; set; }
        public string CreatedBy { get; set; }
        public string ResourceAgencyNumber { get; set; }
        public string Subject { get; set; }
        public string Body { get; set; }
        public string regardingentityname { get; set; }
        public string regardingentityid { get; set; }
        public bool iscc { get; set; }

    }
}
