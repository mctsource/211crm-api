﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _211Models.PostModel
{
    public class OTBMakeRefferalRequest
    {
        public string CaseId { get; set; }
        public string SearchedTerm { get; set; }
        public string ResourceAgencyNumber { get; set; }
        public string ServiceName { get; set; }
        public string Id { get; set; }
    }
}
