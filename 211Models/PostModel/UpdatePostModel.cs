﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _211Models.PostModel
{
    public class UpdatePostModel
    {
        public string NeedGuid { get; set; }
        public string NeedMet { get; set; }
        public string NeedStatusReason { get; set; }
        public string priority { get; set; }
    }
}
