﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _211Models.PostModel
{
    public class AnnotationCase
    {
        public string CaseId { get; set; }
        public string Need { get; set; }
        public string Notes { get; set; }
        public string Annotation { get; set; }
        public string ResourceHistoryName { get; set; }
         
        public string operationflag { get; set; }
        public string annotationguid { get; set; }
        
        public string Category { get; set; }
        public string AgentGuid { get; set; }

        public string PageNumber { get; set; }
        public string PageSize { get; set; }
        public string SortColumn { get; set; }
        public bool SortType { get; set; }
        public bool IsSortingEnabled { get; set; }

    }
}
