﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _211Models.PostModel
{
    public class SuggestionModel
    {
        public string SearchText { get; set; }
        public string SearchType { get; set; }
        
    }
}
