﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _211Models.PostModel
{
    public class TaxonomyDirectionpostModel
    {
        public string TaxonomyCode { get; set; }
        public string TaxonomyLevel { get; set; }
    }
}
