﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _211Models.PostModel
{
    public class AddupdatetermAllpostmodel
    {
        public string CaseId { get; set; }
        public string AddUpdatePossibleFlag { get; set; }
        public string SearchedTerm { get; set; }
        public string resourceguid { get; set; }
        public string NeedMet { get; set; }
        public string NeedStatusReason { get; set; }
        public string ReferralReason { get; set; }
    }
}
