﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _211Models.PostModel
{
    public class CreateUsedTermsRequest
    {
        public string CaseId { get; set; }
        public string SearchedTerm { get; set; }
        public string ResourceAgencyNumber { get; set; }

    }
}
