﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _211Models.PostModel
{
    public class CasePostModel
    {
        public string CaseId { get; set; }
        public string CountryName { get; set; }
        public string CountryGUID { get; set; }
        public string ProvinceName { get; set; }
        public string ProvinceGUID { get; set; }
        public string ProvinceCode { get; set; }


    }
}
