﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _211Models.PostModel
{
    public class ReferralPostModel
    {
        public string ReferralGuid { get; set; }
        public string ReasonGuid{get;set;}
    }
}
