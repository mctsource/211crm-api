﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _211Models.PostModel
{
    public class CategorizedDataViewModel
    {
        public string resourceId { get; set; }
        public string AgentId { get; set; }
        public string resourcetype { get; set; }
        public string resourcefile { get; set; }
    }
}
