﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _211Models.PostModel
{
    public class RemoveRecordRequest
    {
        public string RemoveRecordId { get; set; }
        public string EntityName { get; set; }
    }
}
