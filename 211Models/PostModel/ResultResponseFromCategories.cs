﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _211Models.PostModel
{
    public class ResultResponseFromCategories
    {
        public string CategoryTerm{get;set;}
        public string PageNumber { get; set; }
        public string PageSize { get; set; }
        public string SearchText { get; set; }
        public string SortColumn { get; set; }
        public bool SortType { get; set; }
        public bool IsSortingEnabled { get; set; }
    }
}
