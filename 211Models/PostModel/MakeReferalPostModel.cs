﻿using _211Models.Properties;
using _211Models.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _211Models.PostModel
{
    public class MakeReferalPostModel
    {
        public string AgencyName { get; set; }
        public string CaseId { get; set; }
        public string ResourceAgencyNumber { get; set; }
        public string[] Taxonomyterms { get; set; }
        public string taxonomyterm { get; set; }
        public string termcounts { get; set; }
        public string needmet { get; set; }
        public string needstatus { get; set; }
        public string priority { get; set; }
        public string createdonresources { get; set; }
        public IList<Optionset> Needmetlist { get; set; }
        public IList<Optionset> NeedStatusList { get; set; }
        public IList<Optionset> Prioritylevellist { get; set; }
        public IList<Optionset> referralstatuslist { get; set; }
        public IList<NeedListOfRefferal> Needlist { get; set; }
        public string recordid { get; set; }
        public string createdon { get; set; }
    }
}
