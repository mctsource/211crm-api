﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _211Models.Constants
{
    public static class ConstantMessages
    {
        #region Messages
        public static string ERROR_RECORD_SUCCESSFULLY = "Record Added Successfully";
        public static string ERROR_RECORD_NOT_INSERTED = "Record Not Inserted.";
        public static string MESSAGE_EMAIL_SUCCESSFULLY = "Email found Successfully";
        public static string MESSAGE_EMAIL_FAILED = "Email Not Found";
        public static string MESSAGE_RECORD_NOT_FOUND = "Record Not Found";
        public static string MESSAGE_RECORD_FOUND = "Record Found";
        public static string MESSAGE_RECORD_REMOVED_SUCCESSFULLY = "Record Removed Successfully";
        public static string MESSAGE_EMAIL_SENT_SUCCESSFULLY = "Email Sent Successfully";
        public static string MESSAGE_RECORD_ALREADY_EXISTED = "Record Already Exist";
        #endregion

    }
}
