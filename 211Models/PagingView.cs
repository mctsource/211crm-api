﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _211Models
{
    public class PagingView
    {
        public string totalCount { get; set; }
        public string pageSize { get; set; }
        public string currentPage { get; set; }
        public string totalPages { get; set; }
        public bool previousPage { get; set; }
        public bool nextPage { get; set; }
    }
}
