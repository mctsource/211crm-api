﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _211Models
{
    public class BaseModel
    {
        public string LoggedInUserName { get; set; }
        public string LoggedInId { get; set; }
    }
}
