﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _211Models
{
    public class GlobalSearchPostModel
    {
        public string PageNumber { get; set; }
        public string PageSize { get; set; }
        public string SearchText { get; set; }
        public string SortColumn { get; set; }
        public bool SortType { get; set; }
        public bool IsSortingEnabled { get; set; }
        public string SearchType { get; set; }
        public string[] TaxonomyLevelName { get; set; }
        public string SearchField { get; set; }
        public string[] AgencyStaus { get; set; }
        public string AndOrConditionFlag { get; set; }
        public string[] TaxonomyTermSearch {get;set;}
        public string TaxonomyField { get; set; }
    }
}
