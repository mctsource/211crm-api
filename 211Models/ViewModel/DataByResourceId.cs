﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _211Models.ViewModel
{
    public class DataByResourceId
    {
        public string TaxonomyTerm { get; set; }
        public string ResourceAgancyNum { get; set; }
        public string MailingCountry { get; set; }
    }
}
