﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _211Models.ViewModel
{
    public class Categorization
    {
        public string ProgramName { get; set; }
        public string ProgramNumber { get; set; }
        public string Linked1Code { get; set; }
        public string Linked1Term { get; set; }
        public string Linked2Code { get; set; }
        public string Linked2Term { get; set; }
        public string TaxonomyTerm { get; set; }

        public string TaxonomyTermCode { get; set; }
        public string TaxonomyTerm1 { get; set; }
        public string TaxonomyTerm1field { get; set; }
        public string TaxonomyTerm2 { get; set; }
        public string TaxonomyTerm2field { get; set; }
        public string TaxonomyTerm3 { get; set; }
        public string TaxonomyTerm3field { get; set; }
    }
}
