﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _211Models.ViewModel
{
    public class RecordList
    {
        public string ResourceAgencyNumber { get; set; }
        public string PublicName { get; set; }
        public string AlternetName { get; set; }
        public string OfficialName { get; set; }
        public string TaxomonyLevelName { get; set; }

        public string WebsiteAddress { get; set; }
        public string PhysicalAddress1 { get; set; }
        public string PhysicalAddress2 { get; set; }
        public string PhoneFax { get; set; }
        public string PhoneNumberBusinessLine { get; set; }

        public string ApplicationProcess { get; set; }
        public string HoursOfOperation { get; set; }

        public string Eligibility { get; set; }
        public string FeeStructureSource { get; set; }
        public string AgencyDescription { get; set; }
        public string SourceOfRecord { get; set; }
        public string AgencyStatus { get; set; }
        public string Greenflag { get; set; }
        public string Blueflag { get; set; }
        public string ProximityDistance { get; set; }
        public string DistanceCount { get; set; }
        public string Longitude { get; set; }
        public string Latitude { get; set; }

    }
}
