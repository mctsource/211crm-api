﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _211Models.ViewModel
{
    public class TaxonomyTreeViewModel
    {
        public string TaxonomyLevelName { get; set; }
        public string TaxonomyLevelCode { get; set; }
        public string TaxonomyLevel { get; set; }

        public string ID { get; set; }
    }
}
