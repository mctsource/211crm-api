﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _211Models.ViewModel
{
    public class CaseLocationResponseModel
    {
        public string Country { get; set; }
        public string County { get; set; }
        public string Province { get; set; }
        public string City { get; set; }
        public string PostalCode { get; set; }
        public string CountryGuid { get; set; }
        public string ProvinceGuid { get; set; }
        public string CountyGuid { get; set; }
        public string CityGuid { get; set; }
        public string PostalcodeGuid { get; set; }
        public string ProvinceCode { get; set; }
        public IList<LocationView> Countries { get; set; }
        
        public IList<ProvinceView> Provices { get; set; }
        public IList<CountiesView> Counties { get; set; }
        public IList<CityView> Cities { get; set; }
        //LocationView


    }
}
