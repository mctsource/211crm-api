﻿using _211Models.Properties;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _211Models.ViewModel
{
    public class Annotationlistview
    {
        public IList<Optionset> subcategories { get; set; }
        public IList<AnnotationViewModel> annotationlist { get; set; }

    }
}
