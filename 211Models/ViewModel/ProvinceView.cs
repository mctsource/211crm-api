﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _211Models.ViewModel
{
    public class ProvinceView
    {
        public string ProvinceName { get; set; }
        public string ProvinceGuid { get; set; }
        public string parentLocation { get; set; }
        public string ChildType { get; set; }
        public string ProvinceCode { get; set; }
    }
}
