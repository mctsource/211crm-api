﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _211Models.ViewModel
{
    public class GetUsedTermsList
    {
        public string ResourceAgencyNumber { get; set; }
        public string TaxonomyTerms { get; set; }
        public string Flagforneed { get; set; }
        public string usedTermId { get; set; }

    }
}
