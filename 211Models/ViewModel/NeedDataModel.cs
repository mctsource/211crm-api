﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _211Models.ViewModel
{
   public class NeedDataModel
    {
       
        public string resourceHistoryID { get; set; }
        public string ResourceAgancyNum { get; set; }
        public string DateOfCall { get; set; }
        public string CallReportNum { get; set; }
        public string ReportVersionNum { get; set; }
        public string OrgNum { get; set; }
        public string ResourceAgencyNum { get; set; }
        public string AgencyNamePublic { get; set; }
        public string TaxonomyCode { get; set; }
        public string TaxonomyName { get; set; }
        public string CountryName { get; set; }
        public string StateProvince { get; set; }
        public string CountyName { get; set; }
        public string CityName { get; set; }
        public string PostalCode { get; set; }
        public string AreaCode { get; set; }
        public string PrefixCode { get; set; }
        public string NeedWasUnmet { get; set; }
        public string ReasonIfUnmetOrPartial { get; set; }
        public string Level1Code { get; set; }
        public string Level1Name { get; set; }
        public string Level2Code { get; set; }
        public string Level2Name { get; set; }
        public string Level3Code { get; set; }
        public string Level3Name { get; set; }
        public string Level4Code { get; set; }
        public string Level4Name { get; set; }
        public string Level5Code { get; set; }
        public string Level5Name { get; set; }
        public string PhoneWorkerNum { get; set; }
        public string PhoneWorkerFirstName { get; set; }
        public string PhoneWorkerLastName { get; set; }
        public string AIRSNeedCategory { get; set; }
        public string ReportNeedNum { get; set; }
        public string ParentResourceNum { get; set; }
        public string ParentAgencyName { get; set; }



        

    }
}
