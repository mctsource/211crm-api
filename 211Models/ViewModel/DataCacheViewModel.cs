﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _211Models.ViewModel
{
    public class DataCacheViewModel
    {
        public string LastProgramNumberPreviousPage { get; set; }
        public string LastResourceAgencyNumberCountPreviousPage { get; set; }
        public string TotalNumbeOfResourceAgencyNumberOfPRogram { get; set; }
        public string PageFlag { get; set; }
    }
}
