﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _211Models.ViewModel
{
    public class LocationView
    {
        public string CountryName { get; set; }
        public string CountryGUID { get; set; }
        public string ChildType { get; set; }
        public string ParentLocation { get; set; }
    }
}
