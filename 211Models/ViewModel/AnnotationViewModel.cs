﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using _211Models.Properties;

namespace _211Models.ViewModel
{
    public class AnnotationViewModel
    {
        public string Annotation { get; set; }
        public string Category  { get; set; }
        public string SubCategory { get; set; }
        public string Createdby { get; set; }
        public string ResourcehistoryName { get; set; }
        public string createdon { get; set; }
        public string annotationid { get; set; }
        public string SubCategoryId { get; set; }
        public IList<Optionset> subcategory { get; set; }

    }
}
