﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _211Models.ViewModel
{
    public class CityView
    {
        public string CityName { get; set; }
        public string CityGUID { get; set; }
        public string ParentLocation { get; set; }
        public string ChildType { get; set; }
    }
}
