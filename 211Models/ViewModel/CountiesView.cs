﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _211Models.ViewModel
{
    public class CountiesView
    {
        public string CountyName { get; set; }
        public string CountyGUID { get; set; }
        public string ParentLocation { get; set; }
        public string ChildType { get; set; }
    }
}
