﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _211Models.ViewModel
{
    public class CategoriesResponseModel
    {
        public string CategoryName { get; set; }
      
        public string CategoryCode   { get; set; }
        public string CategoryRecordId { get; set; }
        public IList<CategoriesResponseModel> Childtreevalue { get; set; }

       
    }
}
