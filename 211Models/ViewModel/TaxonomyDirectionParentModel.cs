﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _211Models.ViewModel
{
    public class TaxonomyDirectionParentModel
    {
        public string parenttree { get; set; }
        public string LevelType { get; set; }
        public string levelname { get; set; }
        public string levelcode { get; set; }
        public string Id { get; set; }
        public IList<TaxonomyDirectionParentModel> ChildTree { get; set; }
        

    }
}
