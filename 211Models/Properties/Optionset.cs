﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _211Models.Properties
{
    public class Optionset
    {
        public int id { get; set; }
        public string name { get; set; }
        public string GuidId { get; set; }
    }
}
