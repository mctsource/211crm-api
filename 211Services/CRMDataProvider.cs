﻿using _211Models;
using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Messages;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _211Services
{
    public class CRMDataProvider
    {
        protected ServiceResponse serviceResponse = null;
        protected static IOrganizationService crmSvx;
        //EarlyBound XRM Object
        //protected static Xrm objXrm;
        //public void Dispose()
        //{
        //    if (objXrm != null)
        //    {
        //        objXrm.Dispose();
        //    }

        //}

        /// <summary>
        /// Attach Entity Objects
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="id"></param>
        /// <returns></returns>
        /// 
        //protected T GetAttachedEntity<T>(Guid id) where T : Microsoft.Xrm.Sdk.Entity

        //{
        //    return (objXrm.GetAttachedEntities().Where(e => e.Id == id).FirstOrDefault() as T);
        //}

        /// <summary>
        /// Commong Method For Update Entity Data By Id
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="id"></param>
        /// <param name="update"></param>
        /// <returns></returns>
        //protected T UpdateAttachedEntity<T>(Guid id, Action<T> update) where T : Microsoft.Xrm.Sdk.Entity, new()
        //{
        //    var entity = GetAttachedEntity<T>(id);
        //    if (entity == null)
        //    {
        //        entity = new T();
        //        entity.Id = id;
        //        update(entity);
        //        objXrm.Attach(entity);
        //    }
        //    else
        //    {
        //        update(entity);
        //    }
        //    objXrm.UpdateObject(entity);


        //    return entity;
        //}

        protected static bool CheckResults(SaveChangesResultCollection results)
        {
            return results.HasError;
        }

        public static void BulkCreate(IOrganizationService service, DataCollection<Entity> entities)
        {
            // Create an ExecuteMultipleRequest object.
            var multipleRequest = new ExecuteMultipleRequest()
            {
                // Assign settings that define execution behavior: continue on error, return responses. 
                Settings = new ExecuteMultipleSettings()
                {
                    ContinueOnError = false,
                    ReturnResponses = true
                },
                // Create an empty organization request collection.
                Requests = new OrganizationRequestCollection()
            };

            // Add a CreateRequest for each entity to the request collection.
            foreach (var entity in entities)
            {
                CreateRequest createRequest = new CreateRequest { Target = entity };
                multipleRequest.Requests.Add(createRequest);
            }

            // Execute all the requests in the request collection using a single web method call.
            ExecuteMultipleResponse multipleResponse = (ExecuteMultipleResponse)service.Execute(multipleRequest);

        }
    }
}
