﻿using _211Models;
using _211Models.ViewModel;
using GemBox.Spreadsheet;
using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Query;
using Microsoft.Xrm.Tooling.Connector;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace _211Services
{


    public class OutOfDatabaseService:CRMDataProvider
    {
        CrmServiceClient _client;
        public OutOfDatabaseService()
        {
            serviceResponse = new ServiceResponse();
            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;

            _client = new CrmServiceClient(ConfigurationManager.AppSettings["CRMConnectionString"]);


            crmSvx = (IOrganizationService)_client.OrganizationWebProxyClient != null ?
                            (IOrganizationService)_client.OrganizationWebProxyClient :

                        (IOrganizationService)_client.OrganizationServiceProxy;

        }
        public ServiceResponse OutOfDatabaseReport(DownloadCsvModel downloadCsvModel)
        {

            var csv = new StringBuilder();
            //var heading = string.Format(",Agent ODBR,Agnecy Name,Service Name,Agency Website,Website Searched,Phone Number,Email Address,Address Line 1,Address Line 2,City,Province,Country,Postal Code");
            //csv.AppendLine(heading);

            var heading = string.Format(",CallReportNum,DateOfCall,FirstName,LastName, OODResourceName, OODServiceName, OODNotes, OODPhoneNumber, OODPhysicalAddress, OODWebsiteOfAgency, OODWebsiteSearched, ReportVersionNum");
            csv.AppendLine(heading);

            try
            {



                string fromDate = downloadCsvModel.start_date;
                string toDate = downloadCsvModel.end_date;
                // EntityCollection encryptEntity = getOutofDatabase(_client, fromDate, toDate);

                FetchExpression fethXml = getOutOfDatabase(fromDate, toDate);
                Microsoft.Xrm.Sdk.EntityCollection encryptEntity = _client.RetrieveMultiple(fethXml);

               

                    for (int i = 0; i < encryptEntity.Entities.Count; i++)
                {
                    //String Agent_ODBR = "", Odbid = "", Agnecy_Name = "", Service_Name = "", Agency_Website = "", Website_Searched = "", Phone_Number = "", Email_Address = "", Address_Line_1 = "", Address_Line_2 = "", City = "", Province = "", Country = "", Postal_Code = "";
                    String Call_Report_Num = "", DateOfCall = "",FirstName = "", LastName = "", OODResourceName = "", OODServiceName = "",   OODNotes = "", OODPhoneNumber = "", OODPhysicalAddress = "", OODWebsiteOfAgency = "", OODWebsiteSearched = "", ReportVersionNum = "";
                    EntityReference id2;
                        Microsoft.Xrm.Sdk.Entity entity_new = encryptEntity[i];
                    if (encryptEntity != null)
                    {
                        //EntityReference agentt = entity_new.GetAttribute‌​‌​Value<EntityReference>("ccon_outofdatabasereferralid");
                        EntityReference userReferenceEntity = entity_new.GetAttribute‌​‌​Value<EntityReference>("ccon_agentodbr");
                        if(userReferenceEntity != null)
                        {
                            Guid userguid = userReferenceEntity.Id;
                            Microsoft.Xrm.Sdk.Entity UserEntity = _client.Retrieve("systemuser", userguid, new ColumnSet("firstname", "lastname"));
                          
                            FirstName = UserEntity.GetAttribute‌​‌​Value<string>("firstname");
                            LastName = UserEntity.GetAttribute‌​‌​Value<string>("lastname");
                            
                        }
                        Call_Report_Num = "";
                        //id = entity_new.GetAttribute‌​‌​Value<string>("ccon_odbid");
                        DateOfCall = " " + entity_new.GetAttribute‌​‌​Value<DateTime>("createdon").ToString("yyyy-MM-dd HH:mm:ss");
                       
                        OODResourceName = entity_new.GetAttribute‌​‌​Value<string>("ccon_agency");
                        OODServiceName = entity_new.GetAttribute‌​‌​Value<string>("ccon_servicename");
                        OODNotes = entity_new.GetAttribute‌​‌​Value<string>("ccon_othernotes");
                        OODPhoneNumber = entity_new.GetAttribute‌​‌​Value<string>("ccon_phonenumber");
                        OODPhysicalAddress = entity_new.GetAttribute‌​‌​Value<string>("ccon_addressline2");
                        OODWebsiteOfAgency = entity_new.GetAttribute‌​‌​Value<string>("ccon_agency");
                        OODWebsiteSearched = entity_new.GetAttribute‌​‌​Value<string>("ccon_websitesearched");
                        ReportVersionNum = entity_new.GetAttribute‌​‌​Value<string>("ccon_province");
                        
                        
                        //Country = entity_new.GetAttribute‌​‌​Value<string>("ccon_country");
                        //Postal_Code = entity_new.GetAttribute‌​‌​Value<string>("ccon_postalcode");
                    }

                    var newLine = string.Format("{0},{1},{2},{3},{4},{5},{6},{7},{8},{9},{10},{11}", string.Empty, Call_Report_Num,  DateOfCall, FirstName, LastName, OODResourceName, OODServiceName, OODNotes, OODPhoneNumber, OODPhysicalAddress, OODWebsiteOfAgency, OODWebsiteSearched, ReportVersionNum);

                    csv.AppendLine(newLine);
                }



                HttpContext.Current.Response.Clear();
                HttpContext.Current.Response.Buffer = true;
                HttpContext.Current.Response.AddHeader("content-disposition", "attachment;filename=211_Outofdatabase_" + fromDate + "_To_" + toDate + ".csv");
                HttpContext.Current.Response.Charset = "";
                HttpContext.Current.Response.ContentType = "text/csv";
                HttpContext.Current.Response.Output.Write(csv);
                HttpContext.Current.Response.Flush();
                HttpContext.Current.Response.End();

                serviceResponse.ResponseType = "1";
                serviceResponse.Data = null;
                serviceResponse.RedirectUrl = "";
                serviceResponse.ResponseMessge = "File Downloaded successfully";

                return serviceResponse;

            }
            catch (Exception ex)
            {
                string message = ex.Message;
                throw;
            }


        }

        public static FetchExpression getOutOfDatabase(string startDate, string endDate)
        {
            var fetchXml = $@"<fetch version='1.0' output-format='xml-platform' mapping='logical' distinct='false'>
                          <entity name='ccon_outofdatabasereferral'>
                            <attribute name='ccon_agentodbr' />           
                            <attribute name='ccon_outofdatabasereferralid' />
                            <attribute name='ccon_odbid' />
                            <attribute name='createdon' />                            
                            <attribute name='ccon_agnecyname' />                            
                            <attribute name='ccon_agency' />                            
                            <attribute name='ccon_servicename' />    
                            <attribute name='ccon_othernotes' />  
                            <attribute name='ccon_websitesearched' />                            
                            <attribute name='ccon_phonenumber' />                            
                            <attribute name='ccon_emailaddress' />                            
                            <attribute name='ccon_addressline1' />                            
                            <attribute name='ccon_addressline2' />                            
                            <attribute name='ccon_city' />                            
                            <attribute name='ccon_province' />                            
                            <attribute name='ccon_country' />                            
                            <attribute name='ccon_postalcode' />                            
                                             
                            <filter type='and'>
                              <condition attribute='createdon' operator='on-or-after' value='{startDate}' />
                              <condition attribute='createdon' operator='on-or-before' value='{endDate}' />
                            <condition attribute='ccon_ismakereferral' value='1' operator='eq'/>
                            </filter>
                          </entity>
                        </fetch>";
            return new FetchExpression(fetchXml);
        }


        //public static FetchExpression getOutOfDatabase(string startDate, string endDate)
        //{
        //    var fetchXml = $@"<fetch version='1.0' output-format='xml-platform' mapping='logical' distinct='false'>
        //                  <entity name='ccon_outofdatabasereferral'>
        //                    <attribute name='ccon_outofdatabasereferralid' />
        //                    <attribute name='ccon_odbid' />
        //                    <attribute name='createdon' />                            
        //                    <attribute name='ccon_agnecyname' />                            
        //                    <attribute name='ccon_servicename' />                            
        //                    <attribute name='ccon_othernotes' />                            
        //                    <attribute name='ccon_phonenumber' />                            
        //                    <attribute name='ccon_addressline2' />                            
        //                    <attribute name='ccon_agency' />                            
        //                    <attribute name='ccon_websitesearched' />                            
        //                    <attribute name='ccon_province' />                            
                                                     
        //                    <filter type='and'>
        //                      <condition attribute='createdon' operator='on-or-after' value='{startDate}' />
        //                      <condition attribute='createdon' operator='on-or-before' value='{endDate}' />
        //                    </filter>
        //                  </entity>
        //                </fetch>";
        //    return new FetchExpression(fetchXml);
        //}


    }
}
