﻿using _211Models;
using _211Models.PostModel;
using _211Models.ViewModel;
using GemBox.Spreadsheet;
using Microsoft.Xrm.Client.Services;
using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Query;
using Microsoft.Xrm.Tooling.Connector;
using MongoDB.Bson;
using MongoDB.Bson.Serialization;
using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace _211Services
{
    public class DownloadDatabyResourceId:CRMDataProvider
    {
        CrmServiceClient _client;

        //CrmServiceClient _client;
        public DownloadDatabyResourceId()
        {
            serviceResponse = new ServiceResponse();
            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;

            _client = new CrmServiceClient(ConfigurationManager.AppSettings["CRMConnectionString"]);


            crmSvx = (IOrganizationService)_client.OrganizationWebProxyClient != null ?
                            (IOrganizationService)_client.OrganizationWebProxyClient :

                        (IOrganizationService)_client.OrganizationServiceProxy;

        }

       

        public ServiceResponse ExportResource(DownloadCsvModel downloadCsvModel)
        {
            var csv = new StringBuilder();
            var heading = string.Format(",DateOfCall,CallReportNum,ReportVersionNum,OrgNum,ResourceNum,NamePublic,TaxonomyCode,TaxonomyTerm,CountryName,StateProvince,CountyName,CityName,PostalCode,AreaCode,PrefixCode,MetOrUnmet,ReasonForUnmetNeed,ReferralStatus,ResourceIDInExternalSystem,SiteResourceNum,SiteName,ParentResourceNum,ParentAgencyName");
            csv.AppendLine(heading);
            try
            {
                string fromDate = downloadCsvModel.start_date;
                string toDate = downloadCsvModel.end_date;
                EntityCollection encryptEntity = getEntityNew(_client, fromDate, toDate);
                DataByResourceId resourceDetail = null;

                //resourceDetail = GetDataByResouceIdNew(encryptEntity);
                for (int i = 0; i < encryptEntity.Entities.Count; i++)
                {
                    Microsoft.Xrm.Sdk.Entity entity_new = encryptEntity[i];
                    String resourceId = entity_new.GetAttribute‌​‌​Value<string>("ccon_resourcehistorynumber");
                    String metUnMetReason = entity_new.GetAttribute‌​‌​Value<string>("ccon_needmetunmetreason");
                    String referral_status_string = "";
                    OptionSetValue referral_status = entity_new.GetAttribute‌​‌​Value<OptionSetValue>("ccon_referralstatus");
                    if (referral_status != null)
                    {
                        referral_status_string = entity_new.FormattedValues["ccon_referralstatus"];

                    }
                    String DateOfCall = " " + entity_new.GetAttribute‌​‌​Value<DateTime>("createdon").ToString("yyyy-MM-dd HH:mm:ss");

                    String CallReportNum = "", ReportVersionNum = "", OrgNum = "", ResourceNum = "", NamePublic = "", TaxonomyCode = "", TaxonomyTerm = "", CountryName = "", StateProvince = "", CountyName = "", CityName = "", PostalCode = "", AreaCode = "", PrefixCode = "", MetOrUnmet = "", ReasonForUnmetNeed = "", ReferralStatus = "", ResourceIDInExternalSystem = "", SiteResourceNum = "", SiteName = "", ParentResourceNum = "", ParentAgencyName = "";


                    if (resourceId != null)
                    {
                        //  ServiceResponse serviceResponse = TaxonomyTree(resourceId);
                        // getCentralResourceEntity(resourceId)
                        // string encryptEntityResource = getCentralResourceEntity1(_client, resourceId);
                        EntityCollection encryptEntityResource = getCentralResourceEntity(_client, resourceId);

                        if(encryptEntityResource != null && encryptEntityResource.Entities.Count()>0)
                        {
                        Microsoft.Xrm.Sdk.Entity entity_resource = encryptEntityResource[0];


                        //var abc = resourceDetail.Where(_ => _.ResourceAgencyNum == entity_new.GetAttribute‌​‌​Value<string>("ccon_resourcehistorynumber")).FirstOrDefault();
                        //var abc = resourceDetail.ResourceAgancyNum == entity_new.GetAttributeValue<string> ("ccon_resourcehistorynumber").FirstOrDefault();
                        //    var abc = resourceDetail.Where(_ => _.ResourceAgencyNum == entity_new.GetAttribute‌​‌​Value<string>/("ccon_resourcehistorynumber")).FirstOrDefault();
                        //   if (abc != null)
                        //  {

                        if (encryptEntityResource != null)
                        {
                            NamePublic = entity_resource.GetAttributeValue<String>("ccon_publicname");
                            TaxonomyCode = entity_resource.GetAttributeValue<String>("ccon_taxonomycodes");
                            TaxonomyTerm = entity_resource.GetAttributeValue<String>("ccon_taxonomyterms");
                            CountryName = entity_resource.GetAttributeValue<String>("ccon_mailingcountry");
                            StateProvince = entity_resource.GetAttributeValue<String>("ccon_mailingstateprovince");
                            CountyName = entity_resource.GetAttributeValue<String>("ccon_mailingstateprovince");
                            CityName = entity_resource.GetAttributeValue<String>("ccon_mailingcity");
                            PostalCode = entity_resource.GetAttributeValue<String>("ccon_mailingpostalcode");
                            AreaCode = entity_resource.GetAttributeValue<String>("ccon_mailingpostalcode");
                            MetOrUnmet = metUnMetReason;
                            ReferralStatus = referral_status_string;
                            AreaCode = entity_resource.GetAttributeValue<String>("ccon_mailingpostalcode");
                            ResourceIDInExternalSystem = entity_resource.GetAttributeValue<String>("ccon_resourceagencynum");
                            SiteResourceNum = entity_resource.GetAttributeValue<String>("ccon_resourceagencynum");
                            SiteName = entity_resource.GetAttributeValue<String>("ccon_siteagencynamepublic");
                            ParentResourceNum = entity_resource.GetAttributeValue<String>("ccon_resourceagencynum");
                            ParentAgencyName = entity_resource.GetAttributeValue<String>("ccon_programagencynamepublic");


                            //ParentResourceNum = abc.ResourceAgencyNum;
                            //ParentAgencyName = abc.ProgramAgencyNamePublic;
                            //  }
                        }
                    }
                    var newLine = string.Format("{0},{1},{2},{3},{4},{5},{6},{7},{8},{9},{10},{11},{12},{13},{14},{15},{16},{17},{18},{19},{20},{21},{22},{23},", string.Empty, DateOfCall, CallReportNum, ReportVersionNum, OrgNum, ResourceNum, NamePublic, TaxonomyCode, TaxonomyTerm, CountryName, StateProvince, CountyName, CityName, PostalCode, AreaCode, PrefixCode, MetOrUnmet, ReasonForUnmetNeed, ReferralStatus, ResourceIDInExternalSystem, SiteResourceNum, SiteName, ParentResourceNum, ParentAgencyName);

                    csv.AppendLine(newLine);
                }

                HttpContext.Current.Response.Clear();
                HttpContext.Current.Response.Buffer = true;
                HttpContext.Current.Response.AddHeader("content-disposition", "attachment;filename=211_Refferal_Report" + ".csv");
                HttpContext.Current.Response.Charset = "";
                HttpContext.Current.Response.ContentType = "text/csv";
                HttpContext.Current.Response.Output.Write(csv);
                HttpContext.Current.Response.Flush();
                HttpContext.Current.Response.End();

                serviceResponse.ResponseType = "1";
                serviceResponse.Data = null;
                serviceResponse.RedirectUrl = "";
                serviceResponse.ResponseMessge = "File Downloaded successfully";
            }
            }
            catch (Exception e)
            {

            }


            return serviceResponse;
        }


        public DataByResourceId GetDataByResouceIdNew(EntityCollection resourceId)
        {
            //IList<ResourceDetail> dB_Referrals = new List<ResourceDetail>();
            DataByResourceId dB_Referrals = new DataByResourceId();
            List<DataByResourceId> dB_Referralslist = new List<DataByResourceId>();
            IEnumerable<string> a = resourceId.Entities.Select(_ => _.GetAttributeValue<string>("ccon_resourcehistorynumber"));
            try
            {
                EntityCollection outofdb = new EntityCollection();
                //List<DB_ReferralsPostModel> dB_Referralslist = new List<DB_ReferralsPostModel>();
                QueryExpression outofdbquery = new QueryExpression()
                {

                    EntityName = "ccon_centralresources",
                    ColumnSet = new ColumnSet(true),
                    Criteria = new FilterExpression
                    {
                        FilterOperator = LogicalOperator.And,
                        Conditions =
                    {

                    }
                    }
                };
                outofdb = crmSvx.RetrieveMultiple(outofdbquery);
                if (outofdb.Entities.Count() > 0)
                {
                    for (int j = 0; j < outofdb.Entities.Count(); j++)
                    {

                        dB_Referrals = new DataByResourceId();
                        dB_Referrals.ResourceAgancyNum= outofdb.Entities[j].GetAttributeValue<string>("ccon_resourceagencynum") != null ? outofdb.Entities[j].GetAttributeValue<string>("ccon_resourceagencynum") : "null";
                        dB_Referrals.MailingCountry = outofdb.Entities[j].GetAttributeValue<string>("ccon_mailingcountry") != null ? outofdb.Entities[j].GetAttributeValue<string>("ccon_mailingcountry") : "null";
                        dB_Referrals.TaxonomyTerm = outofdb.Entities[j].GetAttributeValue<string>("ccon_taxonomyterms") != null ? outofdb.Entities[j].GetAttributeValue<string>("ccon_taxonomyterms") : "null";






                        dB_Referralslist.Add(dB_Referrals);
                    }
                   
                }
            }
            catch(Exception ex)
            {
                return null;
            }
            return dB_Referrals;
        }



        public static EntityCollection getEntityNew(IOrganizationService service, string startDate, string endDate)
        {
            QueryExpression queryClaims = new QueryExpression()
            {
                EntityName = "ccon_myresources",
                ColumnSet = new ColumnSet("ccon_resourcehistorynumber", "ccon_needmetunmetreason", "ccon_referralstatus", "createdon"),
                Criteria = new FilterExpression
                {

                    FilterOperator = LogicalOperator.And,
                    Conditions =
                                    {
                                        new ConditionExpression
                                        {
                                            AttributeName = "createdon",
                                            Operator = ConditionOperator.OnOrAfter,
                                            Values = { startDate }
                                        },
                                        new ConditionExpression
                                        {
                                            AttributeName = "createdon",
                                            Operator = ConditionOperator.OnOrBefore,
                                            Values = { endDate }
                                        }
                                    }
                }
            };

            EntityCollection entityCollection = service.RetrieveMultiple(queryClaims);
            return entityCollection;
            //entityCollection.Add(randomnum);

        }

        public static EntityCollection getCentralResourceEntity(IOrganizationService service, string resourceNumber)
        {
            
            QueryExpression queryClaims = new QueryExpression()
            {
                EntityName = "ccon_centralresources",
                ColumnSet = new ColumnSet("ccon_publicname", "ccon_taxonomycodes", "ccon_taxonomyterms", "ccon_mailingcountry", "ccon_mailingstateprovince", "ccon_mailingstateprovince", "ccon_mailingcity", "ccon_mailingpostalcode", "ccon_mailingpostalcode", "ccon_mailingpostalcode", "ccon_resourceagencynum", "ccon_resourceagencynum", "ccon_siteagencynamepublic", "ccon_resourceagencynum", "ccon_programagencynamepublic"),
                Criteria = new FilterExpression
                {

                    FilterOperator = LogicalOperator.And,
                    Conditions =
                                    {
                                        new ConditionExpression
                                        {
                                            AttributeName = "ccon_resourceagencynum",
                                            Operator = ConditionOperator.Equal,
                                            Values = { resourceNumber }
                                        }
                                      
                                    }
                }
            };

            EntityCollection entityCollection = service.RetrieveMultiple(queryClaims);
            return entityCollection;

        }

        

        public ServiceResponse TaxonomyTree(string resourceNumber)
        {
            TaxonomyTreeViewModel taxonomyTreeView = new TaxonomyTreeViewModel();
            List<TaxonomyTreeViewModel> taxonomyTreelist = new List<TaxonomyTreeViewModel>();
            
                EntityCollection treeview = new EntityCollection();
                QueryExpression treeviewquery = new QueryExpression()
                {

                    EntityName = "ccon_centralresources",
                    ColumnSet = new ColumnSet(true),
                    Criteria = new FilterExpression
                    {
                        FilterOperator = LogicalOperator.And,
                        Conditions =
                    {
                         new ConditionExpression
                         {
                             AttributeName = "ResourceAgencyNum",
                             Operator = ConditionOperator.Equal,
                             Values = { 757130001 }
                         }
                    }
                    }
                };
                treeview = _client.RetrieveMultiple(treeviewquery);
            if (treeview.Entities.Count() > 0)
            {
                for (int j = 0; j < treeview.Entities.Count(); j++)
                {
                    taxonomyTreeView = new TaxonomyTreeViewModel();
                    taxonomyTreeView.TaxonomyLevelName = treeview.Entities[j].GetAttributeValue<string>("ResourceAgencyNum") != null ? treeview.Entities[j].GetAttributeValue<string>("ResourceAgencyNum") : "null";
                  
                    taxonomyTreelist.Add(taxonomyTreeView);
                }
                serviceResponse.ResponseType = "1";
                serviceResponse.Data = taxonomyTreelist;
                serviceResponse.ResponseMessge = "Records Found!";
            }
            serviceResponse.Data = treeview;
             
            return serviceResponse;
        }









    }
}
