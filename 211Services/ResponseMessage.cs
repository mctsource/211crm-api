﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _211Services
{
    public class ResponseMessage
    {
        public static string LogginSuccessFull = "Authenticated Successfully..!";
        public static string InValidLogin = "Invalid Username Or Password..!";
        public static string RecordCreatedSuccessFully = "### Created Successfully..!";
        public static string RecordUpdateSuccessFully = "### Updated Successfully..!";
        public static string DuplicateRecordFound = "Duplicate record found For ###..!";
        public static string RequestFailed = "RequestFailed";
        public static string OTPMessage = "OTP Send to your register mobile no and email id..!";
        public static string UserNamePassSetup = "Username & Password Updated Successfully..!";
        public static string OTPNotMatchedMessage = "OTP Not Matched..!";
        public static string DataFoundSuccessfully = "Data Found Successfully..!";
        public static string DataNotFound = "Data Not Found..!";
        public static string PasswordUpdated = "Password Updated Successfully..!";
        public static string OTPMatchedMessage = "OTP Matched..!";
        public static string RecordDeletedSuccessFully = "The ### was Deactivated Successfully..!";
        public static string RecordActiveSuccessFully = "The ### was Activated successfully";
        public static string OldPassWordNotMatched = "Old Password not matched..!";
    }
}
