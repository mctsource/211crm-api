﻿using _211Models;
using _211Models.ViewModel;
using GemBox.Spreadsheet;
using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Query;
using Microsoft.Xrm.Tooling.Connector;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace _211Services
{
   public class DownloadCsvService : CRMDataProvider
    {
        CrmServiceClient _client;
        public DownloadCsvService()
        {
            serviceResponse = new ServiceResponse();
            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;

            _client = new CrmServiceClient(ConfigurationManager.AppSettings["CRMConnectionString"]);


            crmSvx = (IOrganizationService)_client.OrganizationWebProxyClient != null ?
                            (IOrganizationService)_client.OrganizationWebProxyClient :

                        (IOrganizationService)_client.OrganizationServiceProxy;
        }
        public ServiceResponse ExportData(DownloadCsvModel downloadCsvModel)
        {

            var csv = new StringBuilder();
            var heading = string.Format(",CallReportNum,ReportVersion,CallDateAndTimeStart,CallDateAndTimeEnd,Neighborhood,CityName,CountyName,StateProvince,CountyName,PostalCode,ReferralsMade,Campaign,InContact ID,Skill,Chat Answered,Source Portal,Aboriginal (self-identified),Age Category,Age Range,Disability (self-identified),Gender,Immigration Status (self-identified),Inquirers Main Source of Income,Interpreter Required, Language spoken,Newcomer to Canada (last 5 years), Track demographics if Person Needing Assistance is not the inquirer,Aboriginal (self-identified),Age Category,Age Range,Disability (self-identified),Gender,Immigration Status (self-identified),Language spoken,Main Source of Income,Newcomer to Canada (last 5 years), Additional assistance provided if inquirers need not met,Did inquirer follow up on referrals?,If no why?, If yes what was the​result?,Would you use 211 again?,Advocacy offered, Call Not Completed,Contact Method,Emergency/Disaster,Follow-up offered,How did the inquirer know to contact 211 for COVID concerns,How did you hear about 211?,Inquiring on Behalf of,Type of Inquiry, Warm transferred?,211 Central East Region,Additional assistance provided if inquirer not met,Did inquirer follow-up on referrals ?,if no  why?,if yes what was the reason?,Would you use 211 again ?,Follow-up completed with caller?, ,LinkedToCallReportNum,	CallLength,	CallerNum,	CallerName,	CallerMiddleName,	CallerLastName,	PhoneWorkerNum,	PhoneWorkerName,	WasRealCall,	WasHangup,	WasSexCall,	WasWrongNumber,	WasPrankCall,	WasSilentCall,	GeoCode,	GeoAssignment,	CallerAddress,	CensusDivision,	CensusTrack,	CensusReportingArea,	211Region,	PhoneNumberFull,	PhoneExtension,	PhoneType,	ThirdPartyName,	ThirdPartyOrganization,	ThirdPartyPhoneNumber,	ThirdPartyAddress,	ThirdPartyCity,	ThirdPartyCounty,	ThirdPartyStateProvince,	ThirdPartyPostalCode,	Narrative,	VolunteerComments,	Feedback,	CallersFeedback,	TextField2,	TextField3,	TextField4,	TextField5,	TextField6,	TextField7,	TextField8,	TextField9,	TextField10,	EnteredByWorkerNum,	EnteredByName,	EnteredOn,	Supervisor,	Reviewed,	FeedbackStatus,	FeedbackFromPhoneWorkerNum,	FeedbackFromPhoneWorkerName,	IPAddress,	OrgNum,	VolIdentifier,	Caller Needs - Community Services - Other Community Services,	Champlain Elder Abuse Coalition - Dwelling - Other,	Champlain Elder Abuse Coalition - Ethnicity,	Champlain Elder Abuse Coalition - Identify if Senior is living with Abuser - Other,	Champlain Elder Abuse Coalition - Living Arrangements - Other,	Champlain Elder Abuse Coalition - Marital Status - Other,	Champlain Elder Abuse Coalition - Nature of Abuse - Other,	Champlain Elder Abuse Coalition - Suspected Abuser - Other,	Champlain Elder Abuse Response Coalition - Dwelling - Other,	Champlain Elder Abuse Response Coalition - Ethnicity,	Champlain Elder Abuse Response Coalition - Identify if Senior is living with Abuser - Other,	Champlain Elder Abuse Response Coalition - Living Arrangements - Other,	Champlain Elder Abuse Response Coalition - Marital Status - Other,	Champlain Elder Abuse Response Coalition - Nature of Abuse - Other,	Champlain Elder Abuse Response Coalition - Suspected Abuser - Other,	Chat Information - Counselor Response Time,	Chat Information - Visitor Exit Time,	Christmas Program - Type of Request - Other,	Connecting Ottawa - Name of agency calling on behalf of client,	Contact Mode - Email - Source - Other,	Data Follow-Up - Comments,	Follow-Up and Advocacy Info - Comments?,	Follow-Up and Advocacy Info - Preferred time to follow up,	Follow-Up Info - Comments?,	Follow-Up Info - Preferred time to follow up,	For Niagara Region Only: - Homelessness: Client situation,	Homeless Youth Elgin County - Warm Transfer - Other,	iCarol/inContact integration panel - Phone Line being Dialed (DNIS),	Inquiry Information - How did the inquirer know to contact 211 for COVID concerns - Other,	Inquiry Information - How did you hear about 211? - Other,	Inquiry Information - Name of Agency calling on behalf of client or Agency,	Kingston Youth Homelessness Initiative - Where is the youth calling from? - Other,Kingston Youth Homelessness Initiative - Why is the youth homeless? - Other,	Mental Health & Addictions - warm transfer to 211 - Agency which transferred the call,	Pre-Chat Questions - City,	Pre-Chat Questions - Postal code,	School Supplies Program - What school does your child attend,	Senior Transportation - Type of Request - Other,	Victim Services - Caller Identified as - Other,	Victim Services - Does the client live with Abuser - Other,	Victim Services - Ethnicity,	Victim Services - Issues Affecting Caller - Other,	Victim Services - Marital Status - Other,	Victim Services - Suspected Abuser - Other,	Your feedback - Comments,	211 Central East - 211 Central East,	211 Central South - 211 Central South Region,	211 Eastern - 211 Eastern Region,	211 Northern - 211 Northern Region,	211 Region of Inquirer - 211 Region of Inquirer,	211 South West - 211 South West,	Alzheimer Society - Alzheimer Society,	Caller Needs - Arts Culture & Recreation - Arts Culture & Recreation,	Caller Needs - Citizenship & Immigration - Citizenship & Immigration,	Caller Needs - Community Services - Community Services,	Caller Needs - Consumer & Commercial - Consumer & Commercial,	Caller Needs - Disaster - Disaster,	Caller Needs - Education - Education,	Caller Needs - Employment - Employment,	Caller Needs - Food & Meals - Food & Meals,	Caller Needs - Health - Health,	Caller Needs - Housing - Housing,	Caller Needs - Income & Financial Assistance - Income & Financial Assistance,	Caller Needs - Individual & Family Services - Individual & Family Services,	Caller Needs - Information Services - Information Services,	Caller Needs - Legal & Public Safety - Legal & Public Safety,	Caller Needs - Mental Health & Addictions - Mental Health & Addictions,	Caller Needs - Other Federal Government - Other Federal Government,	Caller Needs - Other Municipal Government - Other Municipal Government,	Caller Needs - Other Provincial Government - Other Provincial Government,	Caller Needs - Transportation - Transportation,	Caller Needs - Volunteerism & Donations - Volunteerism & Donations,	Champlain Elder Abuse Coalition - Dwelling,	Champlain Elder Abuse Coalition - Identify if Senior is living with Abuser,	Champlain Elder Abuse Coalition - Living Arrangements,	Champlain Elder Abuse Coalition - Marital Status,	Champlain Elder Abuse Coalition - Nature of Abuse,	Champlain Elder Abuse Coalition - Suspected Abuser,	Champlain Elder Abuse Response Coalition - Dwelling,	Champlain Elder Abuse Response Coalition - Identify if Senior is living with Abuser,	Champlain Elder Abuse Response Coalition - Living Arrangements,	Champlain Elder Abuse Response Coalition - Marital Status,	Champlain Elder Abuse Response Coalition - Nature of Abuse,	Champlain Elder Abuse Response Coalition - Suspected Abuser,	Christmas Program - Type of Call,	Christmas Program - Type of Request,	Connecting Ottawa - Abuse and Family Violence,	Connecting Ottawa - Consumer Law,	Connecting Ottawa - Education Law,	Connecting Ottawa - Employment and Work,	Connecting Ottawa - Family Law,	Connecting Ottawa - Health and Disability,	Connecting Ottawa - Housing Law,	Connecting Ottawa - Human Rights,	Connecting Ottawa - Immigration and Refugee Law,	Connecting Ottawa - Language spoken at home,	Connecting Ottawa - Social Assistance and Pensions,	Connecting Ottawa - Wills and Estates,	Connecting Ottawa - Would caller benefit from volunteer accompaniment or other support to access services to which they have been referred?.	Contact Mode - Email - Source.	Contact Mode - What number did you dial to reach us today?,	CTN - CTN,	Data Follow-Up - Taxonomy,	Demographics of Inquirer - Are you comfortable using the internet?,	Demographics of Inquirer - Do you have access to the internet?,	Elder Abuse - Caller referred to Senior Safety Line (SSL),	Elder Abuse - Relationship of the senior to the alleged abuser,	Elder Abuse - Type of Abuse,	Follow-Up and Advocacy Info - Advocacy offered,	Follow-Up and Advocacy Info - Caller satisfaction survey (SQM),	Follow-Up and Advocacy Info - Did you receive service from referral(s)?,	Follow-Up and Advocacy Info - Follow-up offered,	Follow-Up and Advocacy Info - If not were the referrals appropriate?,	Follow-Up and Advocacy Info - Language,	For Niagara Region Only: - Homelessness: Situation Presented by client,	For Niagara Region Only: - Homelessness: Type of shelter needed,	Holiday Sharing Guide Windsor-Essex - Type of Program,	Homeless Youth Elgin County - Age of Caller,	Homeless Youth Elgin County - Refer to Internal Notes in the Youth Homelessness Protocol Elgin resource,	Homeless Youth Elgin County - Warm Transfer to,	Household Income - Estimated Monthly Household Income for the Individual Requiring Assistance,	Information Services - 211 Northern Region - Information Services - 211 Northern Region,	Inquiry Information - Alberta Wildfire,	Inquiry Information - Alzheimer Society,	Inquiry Information - Call Warm Transferred to Provincial Partner,	Inquiry Information - DSO,	Inquiry Information - Elder Abuse,	Inquiry Information - Endangerment,	Inquiry Information - Referred by (Provincial),	Inquiry Information - Syrian Refugee Call,	Inquiry Information - Syrian Refugee Call - Needs,	Inquiry Information - Transferred,	Inquiry Information - Transferred to (Provincial),	Kingston Youth Homelessness Initiative - Warm transfer to,	Kingston Youth Homelessness Initiative - Where is the youth calling from?,	Kingston Youth Homelessness Initiative - Why is the youth homeless?,	Mental Health & Addictions - warm transfer to 211 - Mental Health & Addictions - warm transfer to 211,	Pre-Chat Questions - Age Range,	Pre-Chat Questions - Gender Identity,	Pre-Chat Questions - How did you hear about us?,	Pre-Chat Questions - Province/Territory,	Regional Projects and Partnerships - 211 Region,	School Supplies Program - Type of Call,	Senior Transportation - Type of Request,	Victim Services -  Caller is identified as,	Victim Services - Does the client live with Abuser,	Victim Services - Issues Affecting Caller,	Victim Services - Marital Status,	Victim Services - Suspected Abuser,	Your feedback - Was the chat service easy to use?,	Your feedback - Was your chat helpful?,	Your feedback - Would you recommend 211 to a friend?");
            csv.AppendLine(heading);


            string fromDate = downloadCsvModel.start_date;
            string toDate = downloadCsvModel.end_date;


            try
            {

                string WasRealCall = "", WasHangup = "", WasSexCall = "", WasWrongNumber = "", WasPrankCall = "", WasSilentCall = "", GeoCode = "", GeoAssignment = "", CensusDivision = "", CensusTrack = "", CensusReportingArea = "", Region = "", PhoneExtension = "", PhoneType = "", ThirdPartyName = "", ThirdPartyOrganization = "", ThirdPartyPhoneNumber = "", ThirdPartyAddress = "", ThirdPartyCity = "", ThirdPartyCounty = "", ThirdPartyStateProvince = "", ThirdPartyPostalCode = "", TextField2 = "", TextField3 = "", TextField4 = "", TextField5 = "", TextField6 = "", TextField7 = "", TextField8 = "", TextField9 = "", TextField10 = "", IPAddress = "", VolIdentifier = "", CallerNeedCommunityServicesOther = "", ChamplainElderAbuseCoalitionDwellingOther = "", ChamplainElderAbuseCoalitionEthnicity = "", ChamplainElderAbuseCoalitionIdentifyifSeniorislivingwithAbuserOther = "", ChamplainElderAbuseCoalitionLivingArrangementsOther = "", ChamplainElderAbuseCoalitionMaritalStatusOther = "", ChamplainElderAbuseCoalitionNatureofAbuseOther = "", ChamplainElderAbuseCoalitionSuspectedAbuserOther = "", ChamplainElderAbuseResponseCoalitionDwellingOther = "", ChamplainElderAbuseResponseCoalitionEthnicity = "", ChamplainElderAbuseResponseCoalitionIdentifyifSeniorislivingwithAbuserOther = "", ChamplainElderAbuseResponseCoalitionLivingArrangementsOther = "", ChamplainElderAbuseResponseCoalitionMaritalStatusOther = "", ChamplainElderAbuseResponseCoalitionNatureofAbuseOther = "", ChamplainElderAbuseResponseCoalitionSuspectedAbuserOther = "", ChatInformationCounselorResponseTime = "", ChatInformationVisitorExitTime = "", ChristmasProgramTypeofRequestOther = "", ConnectingOttawaNameofagencycallingonbehalfofclient = "", ContactModeEmailSourceOther = "", DataFollowUpComments = "", FollowUpandAdvocacyInfoComments = "", FollowUpandAdvocacyInfoPreferredtimetofollowup = "", ForNiagaraRegionOnlyHomelessnessClientsituation = "", HomelessYouthElginCountyWarmTransferOther = "", InquiryInformationHowdidtheinquirerknowtocontact211forCOVIDconcernsOther = "", InquiryInformationHowdidyouhearabout211Other = "", InquiryInformationNameofAgencycallingonbehalfofclientorAgency = "", KingstonYouthHomelessnessInitiativeWhereistheyouthcallingfromOther = "", KingstonYouthHomelessnessInitiativeWhyistheyouthhomelessOther = "", MentalHealthAddictionswarmtransferto211Agencywhichtransferredthecall = "", SchoolSuppliesProgramWhatschooldoesyourchildattend = "", SeniorTransportationTypeofRequestOther = "", VictimServicesCallerIdentifiedasOther = "", VictimServicesDoestheclientlivewithAbuserOther = "", VictimServicesEthnicity = "", VictimServicesIssuesAffectingCallerOther = "", VictimServicesMaritalStatusOther = "", VictimServicesSuspectedAbuserOther = "", YourfeedbackComments = "", CentralEast211CentralEast = "", CentralSouth211CentralSouthRegion = "", Eastern211EasternRegion = "", Northern211NorthernRegion = "", RegionofInquirer211RegionofInquirer = "", SouthWest211SouthWest = "", AlzheimerSocietyAlzheimerSociety = "", CallerNeedsArtsCultureRecreationArtsCultureRecreation = "", CallerNeedsCitizenshipImmigrationCitizenshipImmigration = "", CallerNeedsCommunityServicesCommunityServices = "", CallerNeedsConsumerCommercialConsumerCommercial = "", CallerNeedsDisasterDisaster = "", CallerNeedsEducationEducation = "", CallerNeedsEmploymentEmployment = "", CallerNeedsFoodMealsFoodMeals = "", CallerNeedsHealthHealth = "", CallerNeedsHousingHousing = "", CallerNeedsIncomeFinancialAssistanceIncomeFinancialAssistance = "", CallerNeedsIndividualFamilyServicesIndividualFamilyServices = "", CallerNeedsInformationServicesInformationServices = "", SafetyLegalPublicSafety = "", CallerNeedsMentalHealthAddictionsMentalHealthAddictions = "", CallerNeedsOtherFederalGovernmentOtherFederalGovernment = "", CallerNeedsOtherMunicipalGovernmentOtherMunicipalGovernment = "", CallerNeedsOtherProvincialGovernmentOtherProvincialGovernment = "", CallerNeedsTransportationTransportation = "", CallerNeedsVolunteerismDonationsVolunteerismDonations = "", ChamplainElderAbuseCoalitionDwelling = "", ChamplainElderAbuseCoalitionIdentifyifSeniorislivingwithAbuser = "", ChamplainElderAbuseCoalitionLivingArrangements = "", ChamplainElderAbuseCoalitionMaritalStatus = "", ChamplainElderAbuseCoalitionNatureofAbuse = "", ChamplainElderAbuseCoalitionSuspectedAbuser = "", ChamplainElderAbuseResponseCoalitionDwelling = "", ChamplainElderAbuseResponseCoalitionIdentifyifSeniorislivingwithAbuser = "", ChamplainElderAbuseResponseCoalitionLivingArrangements = "", ChamplainElderAbuseResponseCoalitionMaritalStatus = "", ChamplainElderAbuseResponseCoalitionNatureofAbuse = "", ChamplainElderAbuseResponseCoalitionSuspectedAbuser = "", ChristmasProgramTypeofCall = "", ChristmasProgramTypeofRequest = "", ConnectingOttawaAbuseandFamilyViolence = "", ConnectingOttawaConsumerLaw = "", ConnectingOttawaEducationLaw = "", ConnectingOttawaEmploymentandWork = "", ConnectingOttawaFamilyLaw = "", ConnectingOttawaHealthandDisability = "", ConnectingOttawaHousingLaw = "", ConnectingOttawaHumanRights = "", ConnectingOttawaImmigrationandRefugeeLaw = "", ConnectingOttawaLanguagespokenathome = "", ConnectingOttawaSocialAssistanceandPensions = "", ConnectingOttawaWillsandEstates = "", ConnectingOttawaWouldcallerbenefitfromvolunteer = "", CTNCTN = "", DataFollowUpTaxonomy = "", ElderAbuseCallerreferredtoSeniorSafetyLine = "", ElderAbuseRelationshipoftheseniortotheallegedabuser = "", ElderAbuseTypeofAbuse = "", FollowUpandAdvocacyInfoAdvocacyoffered = "", FollowUpandAdvocacyInfoCallersatisfactionsurvey = "", FollowUpandAdvocacyInfoDidyoureceiveservicefromreferral = "", FollowUpandAdvocacyInfoFollowupoffered = "", FollowUpandAdvocacyInfoIfnotwerethereferralsappropriate = "", FollowUpandAdvocacyInfoLanguage = "", ForNiagaraRegionOnlyHomelessnessSituationPresentedbyclient = "", ForNiagaraRegionOnlyHomelessnessTypeofshelterneeded = "", HolidaySharingGuideWindsorEssexTypeofProgram = "", HomelessYouthElginCountyAgeofCaller = "", HomelessYouthElginCountyRefertoInternalNotes = "", HomelessYouthElginCountyWarmTransferto = "", HouseholdIncomeEstimatedMonthlyHouseholdIncome = "", InformationServices211NorthernRegionInformationServices211NorthernRegion = "", InquiryInformationAlbertaWildfire = "", InquiryInformationAlzheimerSociety = "", InquiryInformationCallWarmTransferredtoProvincialPartner = "", InquiryInformationDSO = "", InquiryInformationElderAbuse = "", InquiryInformationEndangerment = "", InquiryInformationReferredbyProvincial = "", InquiryInformationSyrianRefugeeCall = "", InquiryInformationSyrianRefugeeCallNeeds = "", InquiryInformationTransferredtoProvincial = "", KingstonYouthHomelessnessInitiativeWarmtransferto = "", KingstonYouthHomelessnessInitiativeWhereistheyouthcallingfrom = "", KingstonYouthHomelessnessInitiativeWhyistheyouthhomeless = "", MentalHealthAddictionswarmtransferto211MentalHealthAddictionswarmtransferto211 = "", SchoolSuppliesProgramTypeofCall = "", SeniorTransportationTypeofRequest = "", VictimServicesCallerisidentifiedas = "", VictimServicesDoestheclientlivewithAbuser = "", VictimServicesIssuesAffectingCaller = "", VictimServicesMaritalStatus = "", VictimServicesSuspectedAbuser = "";


                string LinkedToCallReportNum = "", CallLength = "", CallerNum = "", CallerName = "", CallerMiddleName = "", CallerLastName = "Unknown", PhoneWorkerNum = "", PhoneWorkerName = "", CallerAddress = "", PhoneNumberFull = "", Narrative = "", VolunteerComments = "", Feedback = "", CallersFeedback = "", EnteredByWorkerNum = "", EnteredByName = "", EnteredOn = "", Supervisor = "", Reviewed = "", FeedbackStatus = "", FeedbackFromPhoneWorkerNum = "", FeedbackFromPhoneWorkerName = "", OrgNum = "", FollowUpInfoComments = "", FollowUpInfoPreferredtimetofollowup = "", iCarolinContactintegrationpanelPhoneLinebeingDiale = "", PreChatQuestionsCity = "", PreChatQuestionsPostalcode = "", DemographicsofInquirerAreyoucomfortableusingtheinternet = "", DemographicsofInquirerDoyouhaveaccesstotheinternet = "", InquiryInformationTransferred = "", PreChatQuestionsAgeRange = "", PreChatQuestionsGenderIdentity = "", PreChatQuestionsHowdidyouhearaboutus = "", PreChatQuestionsProvinceTerritory = "", RegionalProjectsandPartnerships211Region = "", YourfeedbackWasthechatserviceeasytouse = "", YourfeedbackWasyourchathelpful = "", YourfeedbackWouldyourecommend211toafriend = "";

                EntityCollection encryptEntity = getEntityNew(_client, fromDate, toDate);

                for (int i = 0; i < encryptEntity.Entities.Count; i++)
                {

                    Microsoft.Xrm.Sdk.Entity entity_new = encryptEntity[i];
                    string phonenumber = entity_new.GetAttribute‌​‌​Value<string>("ccon_mobilephone");
                    string caseUniqueID = entity_new.GetAttribute‌​‌​Value<string>("ccon_caseuniqueid");
                    //  Guid caseUniqueID = (Guid)entity_new.Attributes["ccon_caseuniqueid"];
                    //  string caseUniqeIDString = caseUniqueID.ToString();

                    string didinquirerfollowupString = "";
                    string additionalassistanceString = "";
                    string wouldyouuse211againString = "";
                    string ifyeswhatwastheresultString = "";
                    string followupcompletedwithcallerString = "";
                    string ifnowhyString = "";

                    FetchExpression fetchPhone = fethPhoneCall(entity_new.Id);
                    DataCollection<Microsoft.Xrm.Sdk.Entity> fetchEventResult = _client.RetrieveMultiple(fetchPhone).Entities;

                    if (fetchEventResult != null && fetchEventResult.Count > 0)
                    {
                        OptionSetValue didinquirerfollowuponreferrals = fetchEventResult[0].GetAttribute‌​‌Value<OptionSetValue>("ccon_didinquirerfollowuponreferrals");
                        if (didinquirerfollowuponreferrals != null)
                        {
                            didinquirerfollowupString = fetchEventResult[0].FormattedValues["ccon_didinquirerfollowuponreferrals"];

                        }
                        OptionSetValue additionalassistance = fetchEventResult[0].GetAttribute‌​‌​Value<OptionSetValue>("ccon_additionalassistanceprovidedifinquirernot");
                        if (additionalassistance != null)
                        {
                            additionalassistanceString = fetchEventResult[0].FormattedValues["ccon_additionalassistanceprovidedifinquirernot"];

                        }
                        OptionSetValue wouldyouuse211again = fetchEventResult[0].GetAttributeValue<OptionSetValue>("ccon_wouldyouuse211again");
                        if (wouldyouuse211again != null)
                        {
                            wouldyouuse211againString = fetchEventResult[0].FormattedValues["ccon_wouldyouuse211again"];
                        }
                        OptionSetValue followupcompletedwithcaller = fetchEventResult[0].GetAttributeValue<OptionSetValue>("ccon_followupcompletedwithcaller");
                        if (followupcompletedwithcaller != null)
                        {
                            followupcompletedwithcallerString = fetchEventResult[0].FormattedValues["ccon_followupcompletedwithcaller"];
                        }
                        OptionSetValue ifyeswhatwastheresult = fetchEventResult[0].GetAttributeValue<OptionSetValue>("ccon_ifyeswhatwastheresult");
                        if (ifyeswhatwastheresult != null)
                        {
                            ifyeswhatwastheresultString = fetchEventResult[0].FormattedValues["ccon_ifyeswhatwastheresult"];
                        }
                        OptionSetValue ifnowhy = fetchEventResult[0].GetAttributeValue<OptionSetValue>("ccon_ifnowhy");
                        if (ifnowhy != null)
                        {
                            ifnowhyString = fetchEventResult[0].FormattedValues["ccon_ifnowhy"];
                        }

                        FollowUpInfoPreferredtimetofollowup = " " + fetchEventResult[0].GetAttributeValue<DateTime>("scheduledend").ToString("yyyy-MM-dd HH:mm:ss");
                        FollowUpInfoComments = fetchEventResult[0].GetAttributeValue<String>("description");
                    }


                    EntityReference cityName = (entity_new.GetAttribute‌​‌​Value<EntityReference>("new_city"));
                    EntityReference countryName = (entity_new.GetAttribute‌​‌​Value<EntityReference>("new_country"));
                    EntityReference StateProvince = (entity_new.GetAttribute‌​‌​Value<EntityReference>("new_province"));
                    EntityReference CountyName = (entity_new.GetAttribute‌​‌​Value<EntityReference>("new_county"));
                    EntityReference PostalCode = (entity_new.GetAttribute‌​‌​Value<EntityReference>("new_postalcode"));
                    string cityNameString = "";
                    string countryNameString = "";
                    string StateProvinceString = "";
                    string CountyNameString = "";
                    string PostalCodeString = "";
                    string AboriginalString = "";
                    string AgeCategoryString = "";
                    string AgeRangeString = "";
                    string DisabilityString = "";
                    string GenderString = "";
                    OptionSetValue AboriginalOptionSet = entity_new.GetAttribute‌​‌​Value<OptionSetValue>("ccon_aboriginalstatus");
                    OptionSetValue AgeCategoryOptionSet = entity_new.GetAttribute‌​‌​Value<OptionSetValue>("new_agecategory");
                    OptionSetValue AgeRangeOptionSet = entity_new.GetAttribute‌​‌​Value<OptionSetValue>("new_agerange");
                    OptionSetValue DisabilityOptionSet = entity_new.GetAttribute‌​‌​Value<OptionSetValue>("ccon_disability");
                    OptionSetValue GenderOptionSet = entity_new.GetAttribute‌​‌​Value<OptionSetValue>("ccon_gender");
                    if (AboriginalOptionSet != null)
                    {
                        AboriginalString = entity_new.FormattedValues["ccon_aboriginalstatus"];
                    }
                    if (AgeCategoryOptionSet != null)
                    {
                        AgeCategoryString = entity_new.FormattedValues["new_agecategory"];
                    }
                    if (AgeRangeOptionSet != null)
                    {
                        AgeRangeString = entity_new.FormattedValues["new_agerange"];
                    }
                    if (DisabilityOptionSet != null)
                    {
                        DisabilityString = entity_new.FormattedValues["ccon_disability"];
                    }
                    if (GenderOptionSet != null)
                    {
                        GenderString = entity_new.FormattedValues["ccon_gender"];
                    }
                    string ImmigreationString = "";
                    OptionSetValue ImmigrationOptionSet = entity_new.GetAttribute‌​‌​Value<OptionSetValue>("ccon_immigrationstatus");
                    if (ImmigrationOptionSet != null)
                    {
                        ImmigreationString = entity_new.FormattedValues["ccon_immigrationstatus"];
                    }
                    string InquireIncomeString = "";
                    OptionSetValue InquireIncomeOptionSet = entity_new.GetAttribute‌​‌​Value<OptionSetValue>("ccon_inquirersmainsourceofincome");
                    if (InquireIncomeOptionSet != null)
                    {
                        InquireIncomeString = entity_new.FormattedValues["ccon_inquirersmainsourceofincome"];
                    }
                    string InterpreterString = "";
                    Boolean InterpreterOptionSet = entity_new.GetAttribute‌​‌​Value<Boolean>("new_interpreterrequired");
                    if (InterpreterOptionSet != null)
                    {
                        InterpreterString = entity_new.FormattedValues["new_interpreterrequired"];
                    }
                    string NewComerString = "";
                    Boolean NewComerOptionSet = entity_new.GetAttribute‌​‌​Value<Boolean>("ccon_new");
                    if (NewComerOptionSet != null)
                    {
                        NewComerString = entity_new.FormattedValues["ccon_new"];
                    }
                    string TrackString = "";
                    Boolean TrackOptionSet = entity_new.GetAttribute‌​‌​Value<Boolean>("ccon_track");
                    if (TrackOptionSet != null)
                    {
                        TrackString = entity_new.FormattedValues["ccon_track"];
                    }
                    string LanguageKnownString = "";
                    OptionSetValue LanguageKnownOptionSet = entity_new.GetAttribute‌​‌​Value<OptionSetValue>("new_languagespoken");
                    if (LanguageKnownOptionSet != null)
                    {
                        LanguageKnownString = entity_new.FormattedValues["new_languagespoken"];
                    }
                    string AboriginalstatusString = "";
                    OptionSetValue AboriginalstatusOptionSet = entity_new.GetAttribute‌​‌​Value<OptionSetValue>("ccon_aboriginalselfidentified");
                    if (AboriginalstatusOptionSet != null)
                    {
                        AboriginalstatusString = entity_new.FormattedValues["ccon_aboriginalselfidentified"];
                    }
                    string PAgeCateGoryString = "";
                    OptionSetValue PAgeCateGoryOptionSet = entity_new.GetAttribute‌​‌​Value<OptionSetValue>("ccon_agecategory");
                    if (PAgeCateGoryOptionSet != null)
                    {
                        PAgeCateGoryString = entity_new.FormattedValues["ccon_agecategory"];
                    }
                    string PAgeRangeString = "";
                    OptionSetValue PAgeRangeOptionSet = entity_new.GetAttribute‌​‌​Value<OptionSetValue>("ccon_agerange");
                    if (PAgeRangeOptionSet != null)
                    {
                        PAgeRangeString = entity_new.FormattedValues["ccon_agerange"];
                    }
                    string PDisabilityString = "";
                    OptionSetValue PDisabilityOptionSet = entity_new.GetAttribute‌​‌​Value<OptionSetValue>("ccon_disabilityselfidentified");
                    if (PDisabilityOptionSet != null)
                    {
                        PDisabilityString = entity_new.FormattedValues["ccon_disabilityselfidentified"];
                    }
                    string PGenderString = "";
                    OptionSetValue PGenderOptionSet = entity_new.GetAttribute‌​‌​Value<OptionSetValue>("ccon_gender1");
                    if (PGenderOptionSet != null)
                    {
                        PGenderString = entity_new.FormattedValues["ccon_gender1"];
                    }
                    string PImmigrationString = "";
                    OptionSetValue PImmigrationOptionSet = entity_new.GetAttribute‌​‌​Value<OptionSetValue>("ccon_immigrationstatusselfidentified");
                    if (PImmigrationOptionSet != null)
                    {
                        PImmigrationString = entity_new.FormattedValues["ccon_immigrationstatusselfidentified"];
                    }
                    string PLanguageSpokenString = "";
                    OptionSetValue PLanguageSpokenOptionSet = entity_new.GetAttribute‌​‌​Value<OptionSetValue>("ccon_languagespoken");
                    if (PLanguageSpokenOptionSet != null)
                    {
                        PLanguageSpokenString = entity_new.FormattedValues["ccon_languagespoken"];
                    }
                    string PMainSourceString = "";
                    OptionSetValue PMainSourceOptionSet = entity_new.GetAttribute‌​‌​Value<OptionSetValue>("ccon_mainsourceofincome");
                    if (PMainSourceOptionSet != null)
                    {
                        PMainSourceString = entity_new.FormattedValues["ccon_mainsourceofincome"];
                    }
                    string PNewComerString = "";
                    Boolean PNewComerOptionSet = entity_new.GetAttribute‌​‌​Value<Boolean>("ccon_newcomer");
                    if (PNewComerOptionSet != null)
                    {
                        PNewComerString = entity_new.FormattedValues["ccon_newcomer"];
                    }
                    string AdvocacyString = "";
                    OptionSetValue AdvocacyOptionSet = entity_new.GetAttribute‌​‌​Value<OptionSetValue>("ccon_advocacyoffered");
                    if (AdvocacyOptionSet != null)
                    {
                        AdvocacyString = entity_new.FormattedValues["ccon_advocacyoffered"];
                    }
                    string CallNotCompleteString = "";
                    Boolean CallNotCompleteOptionSet = entity_new.GetAttribute‌​‌​Value<Boolean>("ccon_callnotcompleted");
                    if (CallNotCompleteOptionSet != null)
                    {
                        CallNotCompleteString = entity_new.FormattedValues["ccon_callnotcompleted"];
                    }
                    string ContactMethodString = "";
                    OptionSetValue ContactMethodOptionSet = entity_new.GetAttribute‌​‌​Value<OptionSetValue>("ccon_contactmethod");
                    if (ContactMethodOptionSet != null)
                    {
                        ContactMethodString = entity_new.FormattedValues["ccon_contactmethod"];
                    }
                    string EmergencyString = "";
                    OptionSetValue EmergencyOptionSet = entity_new.GetAttribute‌​‌​Value<OptionSetValue>("ccon_emergencydisaster");
                    if (EmergencyOptionSet != null)
                    {
                        EmergencyString = entity_new.FormattedValues["ccon_emergencydisaster"];
                    }
                    string FollowupOfferedString = "";
                    OptionSetValue FollowupOfferedOptionSet = entity_new.GetAttribute‌​‌​Value<OptionSetValue>("ccon_followupoffered");
                    if (FollowupOfferedOptionSet != null)
                    {
                        FollowupOfferedString = entity_new.FormattedValues["ccon_followupoffered"];
                    }
                    string HowDidCoviString = "";
                    OptionSetValue HowDidCoviOptionSet = entity_new.GetAttribute‌​‌​Value<OptionSetValue>("ccon_howdid");
                    if (HowDidCoviOptionSet != null)
                    {
                        HowDidCoviString = entity_new.FormattedValues["ccon_howdid"];
                    }
                    string HowYouHearString = "";
                    OptionSetValue HowYouHearOptionSet = entity_new.GetAttribute‌​‌​Value<OptionSetValue>("ccon_howyouhearabout211");
                    if (HowYouHearOptionSet != null)
                    {
                        HowYouHearString = entity_new.FormattedValues["ccon_howyouhearabout211"];
                    }
                    string InquryOnBehalfString = "";
                    OptionSetValue InquryOnBehalfOptionSet = entity_new.GetAttribute‌​‌​Value<OptionSetValue>("ccon_inquiringonbehalfof");
                    if (InquryOnBehalfOptionSet != null)
                    {
                        InquryOnBehalfString = entity_new.FormattedValues["ccon_inquiringonbehalfof"];
                        InquryOnBehalfString = InquryOnBehalfString.Replace(',', ' ');
                    }
                    string InquryTypeString = "";
                    OptionSetValue InquryTypeOptionSet = entity_new.GetAttribute‌​‌​Value<OptionSetValue>("ccon_typeofinquiry");
                    if (InquryTypeOptionSet != null)
                    {
                        InquryTypeString = entity_new.FormattedValues["ccon_typeofinquiry"];
                        InquryTypeString = InquryTypeString.Replace(',', ' ');
                    }
                    string WarmTransferString = "";
                    OptionSetValue WarmTransferOptionSet = entity_new.GetAttribute‌​‌​Value<OptionSetValue>("ccon_warmtransfer");
                    if (WarmTransferOptionSet != null)
                    {
                        WarmTransferString = entity_new.FormattedValues["ccon_warmtransfer"];
                    }
                    if (cityName != null)
                    {
                        cityNameString = cityName.Name;
                    }
                    if (countryName != null)
                    {
                        countryNameString = countryName.Name;
                    }
                    if (StateProvince != null)
                    {
                        StateProvinceString = StateProvince.Name;
                    }
                    if (CountyName != null)
                    {
                        CountyNameString = CountyName.Name;
                    }
                    if (PostalCode != null)
                    {
                        PostalCodeString = PostalCode.Name;
                    }
                    CallerName = entity_new.GetAttributeValue<string>("ccon_firstname");

                    CallerLastName = entity_new.Attributes.Contains("ccon_lastname") ? (entity_new.GetAttribute‌​‌​Value<string>("ccon_lastname")) : "Unknown";
                    CallerAddress = entity_new.GetAttributeValue<string>("new_home") + " " + entity_new.GetAttributeValue<string>("new_apartment") + " " + entity_new.GetAttributeValue<string>("new_addressline1");
                    PhoneNumberFull = entity_new.GetAttributeValue<string>("ccon_mobilephone");
                    Feedback = entity_new.GetAttributeValue<string>("ccon_workerscomments");
                    CallersFeedback = entity_new.GetAttributeValue<string>("ccon_callersfeedback");

                    OptionSetValue areyoucomfortableOptionSet = entity_new.GetAttribute‌​‌​Value<OptionSetValue>("ccon_areyoucomfortableusingtheinternet");
                    if (areyoucomfortableOptionSet != null)
                    {
                        DemographicsofInquirerAreyoucomfortableusingtheinternet = entity_new.FormattedValues["ccon_areyoucomfortableusingtheinternet"];
                    }
                    OptionSetValue douhaveaccessOptionSet = entity_new.GetAttribute‌​‌​Value<OptionSetValue>("ccon_douhaveaccesstotheinternet");
                    if (douhaveaccessOptionSet != null)
                    {
                        DemographicsofInquirerDoyouhaveaccesstotheinternet = entity_new.FormattedValues["ccon_douhaveaccesstotheinternet"];
                    }
                    OptionSetValue agerangeOptionSet = entity_new.GetAttribute‌​‌​Value<OptionSetValue>("new_agerange");
                    if (agerangeOptionSet != null)
                    {
                        PreChatQuestionsAgeRange = entity_new.FormattedValues["new_agerange"];
                    }
                    OptionSetValue genderOptionSet = entity_new.GetAttribute‌​‌​Value<OptionSetValue>("ccon_gender");
                    if (genderOptionSet != null)
                    {
                        PreChatQuestionsGenderIdentity = entity_new.FormattedValues["ccon_gender"];
                    }
                    OptionSetValue howyouhearOptionSet = entity_new.GetAttribute‌​‌​Value<OptionSetValue>("ccon_howyouhearabout211");
                    if (howyouhearOptionSet != null)
                    {
                        PreChatQuestionsHowdidyouhearaboutus = entity_new.FormattedValues["ccon_howyouhearabout211"];
                    }
                    PreChatQuestionsProvinceTerritory = StateProvinceString;
                    OptionSetValue warmTransferotherOptionSet = entity_new.GetAttribute‌​‌​Value<OptionSetValue>("ccon_warmtransfer");
                    if (warmTransferotherOptionSet != null)
                    {
                        HomelessYouthElginCountyWarmTransferOther = entity_new.FormattedValues["ccon_warmtransfer"];
                    }
                    OptionSetValue warmTransfertoOptionSet = entity_new.GetAttribute‌​‌​Value<OptionSetValue>("ccon_warmtransferto1");
                    if (warmTransfertoOptionSet != null)
                    {
                        HomelessYouthElginCountyWarmTransferto = entity_new.FormattedValues["ccon_warmtransferto1"];
                    }


                    EntityReference AgentDetail = (entity_new.GetAttribute‌​‌​Value<EntityReference>("ccon_axdetails"));
                    string agentGuid = "GUID";
                    string caseStartDateString = "";
                    string caseEndDateString = "";
                    string agentDetailName = "";
                    string agentIncontactId = "";
                    EntityReference agentSkill = null;
                    EntityReference campaignid = null;
                    string agentSkillName = "";
                    if (AgentDetail != null)
                    {
                        agentGuid = AgentDetail.Id.ToString();
                        agentDetailName = AgentDetail.Name;

                        Microsoft.Xrm.Sdk.Entity AgentDetailEntity = _client.Retrieve("ccon_axagentdetails", AgentDetail.Id, new ColumnSet("ccon_startime", "ccon_endtime", "ccon_incontactid", "ccon_skills", "ccon_length", "ccon_contactnumber", "createdby", "ccon_phonelinebeingdialeddnis", "ccon_campaignid"));
                        DateTime caseStartDate = AgentDetailEntity.GetAttributeValue<DateTime>("ccon_startime");
                        DateTime caseEndDate = AgentDetailEntity.GetAttributeValue<DateTime>("ccon_endtime");
                        agentIncontactId = AgentDetailEntity.GetAttributeValue<string>("ccon_incontactid");
                        agentSkill = AgentDetailEntity.GetAttributeValue<EntityReference>("ccon_skills");
                        campaignid = AgentDetailEntity.GetAttributeValue<EntityReference>("ccon_campaignid");
                        if (campaignid != null)
                        {
                            if (campaignid.Name.Equals("Chat"))
                            {
                                PreChatQuestionsCity = cityNameString;
                                PreChatQuestionsPostalcode = PostalCodeString;
                            }
                        }

                        if (agentSkill != null)
                        {
                            agentSkillName = agentSkill.Name;
                        }
                        if (caseStartDate != null)
                        {
                            caseStartDateString = " " + caseStartDate.ToString("yyyy-MM-dd HH:mm:ss");

                        }
                        if (caseEndDate != null)
                        {
                            caseEndDateString = " " + caseEndDate.ToString("yyyy-MM-dd HH:mm:ss");

                        }

                        CallLength = AgentDetailEntity.GetAttributeValue<int>("ccon_length").ToString();
                        CallerNum = AgentDetailEntity.GetAttributeValue<string>("ccon_contactnumber");

                        Guid createdByGuid = (AgentDetailEntity.GetAttribute‌​‌​Value<EntityReference>("createdby")).Id;
                        PhoneWorkerName = (AgentDetailEntity.GetAttribute‌​‌​Value<EntityReference>("createdby")).Name;
                        EnteredByName = (AgentDetailEntity.GetAttribute‌​‌​Value<EntityReference>("createdby")).Name;
                        Microsoft.Xrm.Sdk.Entity UserEntity = _client.Retrieve("systemuser", createdByGuid, new ColumnSet("firstname", "lastname", "cdx_agentid", "createdon"));
                        PhoneWorkerNum = UserEntity.GetAttribute‌​‌​Value<string>("cdx_agentid");
                        EnteredByWorkerNum = UserEntity.GetAttribute‌​‌​Value<string>("cdx_agentid");
                        DateTime enteredDate = UserEntity.GetAttribute‌​‌​Value<DateTime>("createdon");
                        EnteredOn = " " + enteredDate.ToString("yyyy-MM-dd HH:mm:ss");
                        Reviewed = "No";
                        iCarolinContactintegrationpanelPhoneLinebeingDiale = AgentDetailEntity.GetAttributeValue<string>("ccon_phonelinebeingdialeddnis");

                    }
                    var newLine = string.Format("{0},{1},{2},{3},{4},{5},{6},{7},{8},{9},{10},{11},{12},{13},{14},{15},{16},{17},{18},{19},{20},{21},{22},{23},{24},{25},{26},{27},{28},{29},{30},{31},{32},{33},{34},{35},{36},{37},{38},{39},{40},{41},{42},{43},{44},{45},{46},{47},{48},{49},{50},{51},{52},{53},{54},{55},{56},{57},{58},{59},{60},{61},{62},{63},{64},{65},{66},{67},{68},{69},{70},{71},{72},{73},{74},{75},{76},{77},{78},{79},{80},{81},{82},{83},{84},{85},{86},{87},{88},{89},{90},{91},{92},{93},{94},{95},{96},{97},{98},{99},{100},{101},{102},{103},{104},{105},{106},{107},{108},{109},{110},{111},{112},{113},{114},{115},{116},{117},{118},{119},{120},{121},{122},{123},{124},{125},{126},{127},{128},{129},{130},{131},{132},{133},{134},{135},{136},{137},{138},{139},{140},{141},{142},{143},{144},{145},{146},{147},{148},{149},{150},{151},{152},{153},{154},{155},{156},{157},{158},{159},{160},{161},{162},{163},{164},{165},{166},{167},{168},{169},{170},{171},{172},{173},{174},{175},{176},{177},{178},{179},{180},{181},{182},{183},{184},{185},{186},{187},{188},{189},{190},{191},{192},{193},{194},{195},{196},{197},{198},{199},{200},{201},{202},{203},{204},{205},{206},{207},{208},{209},{210},{211},{212},{213},{214},{215},{216},{217},{218},{219},{220},{221},{222},{223},{224},{225},{226},{227},{228},{229},{230},{231},{232},{233},{234},{235},{236},{237},{238},{239},{240},{241},{242},{243},{244},{245},{246},{247},{248},{249},{250},{251},{252},{253},{254},{255},{256},{257},{258},{259},{260},{261},{262},{263},{264}{265}", string.Empty, phonenumber, string.Empty, caseStartDateString, caseEndDateString, string.Empty, cityNameString, countryNameString, StateProvinceString, CountyNameString, PostalCodeString, string.Empty, agentDetailName, agentIncontactId, agentSkillName, string.Empty, string.Empty, AboriginalString, AgeCategoryString, AgeRangeString, DisabilityString, GenderString, ImmigreationString, InquireIncomeString, InterpreterString, LanguageKnownString, NewComerString, TrackString, AboriginalstatusString, PAgeCateGoryString, PAgeRangeString, PDisabilityString, PGenderString, PImmigrationString, PLanguageSpokenString, PMainSourceString, PNewComerString, string.Empty, string.Empty, string.Empty, string.Empty, string.Empty, AdvocacyString, CallNotCompleteString, ContactMethodString, EmergencyString, FollowupOfferedString, HowDidCoviString, HowYouHearString, InquryOnBehalfString, InquryTypeString, WarmTransferString, string.Empty, additionalassistanceString, didinquirerfollowupString, ifnowhyString, ifyeswhatwastheresultString, wouldyouuse211againString, followupcompletedwithcallerString, string.Empty, LinkedToCallReportNum, CallLength, CallerNum, CallerName, CallerMiddleName, CallerLastName, PhoneWorkerNum, PhoneWorkerName, WasRealCall, WasHangup, WasSexCall, WasWrongNumber, WasPrankCall, WasSilentCall, GeoCode, GeoAssignment, CallerAddress, CensusDivision, CensusTrack, CensusReportingArea, Region, PhoneNumberFull, PhoneExtension, PhoneType, ThirdPartyName, ThirdPartyOrganization, ThirdPartyPhoneNumber, ThirdPartyAddress, ThirdPartyCity, ThirdPartyCounty, ThirdPartyStateProvince, ThirdPartyPostalCode, Narrative, VolunteerComments, Feedback, CallersFeedback, TextField2, TextField3, TextField4, TextField5, TextField6, TextField7, TextField8, TextField9, TextField10, EnteredByWorkerNum, EnteredByName, EnteredOn, Supervisor, Reviewed, FeedbackStatus, FeedbackFromPhoneWorkerNum, FeedbackFromPhoneWorkerName, IPAddress, OrgNum, VolIdentifier, CallerNeedCommunityServicesOther, ChamplainElderAbuseCoalitionDwellingOther, ChamplainElderAbuseCoalitionEthnicity, ChamplainElderAbuseCoalitionIdentifyifSeniorislivingwithAbuserOther, ChamplainElderAbuseCoalitionLivingArrangementsOther, ChamplainElderAbuseCoalitionMaritalStatusOther, ChamplainElderAbuseCoalitionNatureofAbuseOther, ChamplainElderAbuseCoalitionSuspectedAbuserOther, ChamplainElderAbuseResponseCoalitionDwellingOther, ChamplainElderAbuseResponseCoalitionEthnicity, ChamplainElderAbuseResponseCoalitionIdentifyifSeniorislivingwithAbuserOther, ChamplainElderAbuseResponseCoalitionLivingArrangementsOther, ChamplainElderAbuseResponseCoalitionMaritalStatusOther, ChamplainElderAbuseResponseCoalitionNatureofAbuseOther, ChamplainElderAbuseResponseCoalitionSuspectedAbuserOther, ChatInformationCounselorResponseTime, ChatInformationVisitorExitTime, ChristmasProgramTypeofRequestOther, ConnectingOttawaNameofagencycallingonbehalfofclient, ContactModeEmailSourceOther, DataFollowUpComments, FollowUpandAdvocacyInfoComments, FollowUpandAdvocacyInfoPreferredtimetofollowup, FollowUpInfoComments, FollowUpInfoPreferredtimetofollowup, ForNiagaraRegionOnlyHomelessnessClientsituation, HomelessYouthElginCountyWarmTransferOther, iCarolinContactintegrationpanelPhoneLinebeingDiale, InquiryInformationHowdidtheinquirerknowtocontact211forCOVIDconcernsOther, InquiryInformationHowdidyouhearabout211Other, InquiryInformationNameofAgencycallingonbehalfofclientorAgency, KingstonYouthHomelessnessInitiativeWhereistheyouthcallingfromOther, KingstonYouthHomelessnessInitiativeWhyistheyouthhomelessOther, MentalHealthAddictionswarmtransferto211Agencywhichtransferredthecall, PreChatQuestionsCity, PreChatQuestionsPostalcode, SchoolSuppliesProgramWhatschooldoesyourchildattend, SeniorTransportationTypeofRequestOther, VictimServicesCallerIdentifiedasOther, VictimServicesDoestheclientlivewithAbuserOther, VictimServicesEthnicity, VictimServicesIssuesAffectingCallerOther, VictimServicesMaritalStatusOther, VictimServicesSuspectedAbuserOther, YourfeedbackComments, CentralEast211CentralEast, CentralSouth211CentralSouthRegion, Eastern211EasternRegion, Northern211NorthernRegion, RegionofInquirer211RegionofInquirer, SouthWest211SouthWest, AlzheimerSocietyAlzheimerSociety, CallerNeedsArtsCultureRecreationArtsCultureRecreation, CallerNeedsCitizenshipImmigrationCitizenshipImmigration, CallerNeedsCommunityServicesCommunityServices, CallerNeedsConsumerCommercialConsumerCommercial, CallerNeedsDisasterDisaster, CallerNeedsEducationEducation, CallerNeedsEmploymentEmployment, CallerNeedsFoodMealsFoodMeals, CallerNeedsHealthHealth, CallerNeedsHousingHousing, CallerNeedsIncomeFinancialAssistanceIncomeFinancialAssistance, CallerNeedsIndividualFamilyServicesIndividualFamilyServices, CallerNeedsInformationServicesInformationServices, SafetyLegalPublicSafety, CallerNeedsMentalHealthAddictionsMentalHealthAddictions, CallerNeedsOtherFederalGovernmentOtherFederalGovernment, CallerNeedsOtherMunicipalGovernmentOtherMunicipalGovernment, CallerNeedsOtherProvincialGovernmentOtherProvincialGovernment, CallerNeedsTransportationTransportation, CallerNeedsVolunteerismDonationsVolunteerismDonations, ChamplainElderAbuseCoalitionDwelling, ChamplainElderAbuseCoalitionIdentifyifSeniorislivingwithAbuser, ChamplainElderAbuseCoalitionLivingArrangements, ChamplainElderAbuseCoalitionMaritalStatus, ChamplainElderAbuseCoalitionNatureofAbuse, ChamplainElderAbuseCoalitionSuspectedAbuser, ChamplainElderAbuseResponseCoalitionDwelling, ChamplainElderAbuseResponseCoalitionIdentifyifSeniorislivingwithAbuser, ChamplainElderAbuseResponseCoalitionLivingArrangements, ChamplainElderAbuseResponseCoalitionMaritalStatus, ChamplainElderAbuseResponseCoalitionNatureofAbuse, ChamplainElderAbuseResponseCoalitionSuspectedAbuser, ChristmasProgramTypeofCall, ChristmasProgramTypeofRequest, ConnectingOttawaAbuseandFamilyViolence, ConnectingOttawaConsumerLaw, ConnectingOttawaEducationLaw, ConnectingOttawaEmploymentandWork, ConnectingOttawaFamilyLaw, ConnectingOttawaHealthandDisability, ConnectingOttawaHousingLaw, ConnectingOttawaHumanRights, ConnectingOttawaImmigrationandRefugeeLaw, ConnectingOttawaLanguagespokenathome, ConnectingOttawaSocialAssistanceandPensions, ConnectingOttawaWillsandEstates, ConnectingOttawaWouldcallerbenefitfromvolunteer, CTNCTN, DataFollowUpTaxonomy, DemographicsofInquirerAreyoucomfortableusingtheinternet, DemographicsofInquirerDoyouhaveaccesstotheinternet, ElderAbuseCallerreferredtoSeniorSafetyLine, ElderAbuseRelationshipoftheseniortotheallegedabuser, ElderAbuseTypeofAbuse, FollowUpandAdvocacyInfoAdvocacyoffered, FollowUpandAdvocacyInfoCallersatisfactionsurvey, FollowUpandAdvocacyInfoDidyoureceiveservicefromreferral, FollowUpandAdvocacyInfoFollowupoffered, FollowUpandAdvocacyInfoIfnotwerethereferralsappropriate, FollowUpandAdvocacyInfoLanguage, ForNiagaraRegionOnlyHomelessnessSituationPresentedbyclient, ForNiagaraRegionOnlyHomelessnessTypeofshelterneeded, HolidaySharingGuideWindsorEssexTypeofProgram, HomelessYouthElginCountyAgeofCaller, HomelessYouthElginCountyRefertoInternalNotes, HomelessYouthElginCountyWarmTransferto, HouseholdIncomeEstimatedMonthlyHouseholdIncome, InformationServices211NorthernRegionInformationServices211NorthernRegion, InquiryInformationAlbertaWildfire, InquiryInformationAlzheimerSociety, InquiryInformationCallWarmTransferredtoProvincialPartner, InquiryInformationDSO, InquiryInformationElderAbuse, InquiryInformationEndangerment, InquiryInformationReferredbyProvincial, InquiryInformationSyrianRefugeeCall, InquiryInformationSyrianRefugeeCallNeeds, InquiryInformationTransferred, InquiryInformationTransferredtoProvincial, KingstonYouthHomelessnessInitiativeWarmtransferto, KingstonYouthHomelessnessInitiativeWhereistheyouthcallingfrom, KingstonYouthHomelessnessInitiativeWhyistheyouthhomeless, MentalHealthAddictionswarmtransferto211MentalHealthAddictionswarmtransferto211, PreChatQuestionsAgeRange, PreChatQuestionsGenderIdentity, PreChatQuestionsHowdidyouhearaboutus, PreChatQuestionsProvinceTerritory, RegionalProjectsandPartnerships211Region, SchoolSuppliesProgramTypeofCall, SeniorTransportationTypeofRequest, VictimServicesCallerisidentifiedas, VictimServicesDoestheclientlivewithAbuser, VictimServicesIssuesAffectingCaller, VictimServicesMaritalStatus, VictimServicesSuspectedAbuser, YourfeedbackWasthechatserviceeasytouse, YourfeedbackWasyourchathelpful, YourfeedbackWouldyourecommend211toafriend);


                    csv.AppendLine(newLine);
                }



                HttpContext.Current.Response.Clear();
                HttpContext.Current.Response.Buffer = true;
                HttpContext.Current.Response.AddHeader("content-disposition", "attachment;filename=211_Full_Report_" + fromDate + "_To_" + toDate + ".csv");
                HttpContext.Current.Response.Charset = "";
                HttpContext.Current.Response.ContentType = "text/csv";
                HttpContext.Current.Response.Output.Write(csv);
                HttpContext.Current.Response.Flush();
                HttpContext.Current.Response.End();

                //  File.WriteAllText(@"E:\CRM\CRM Projects\211 Call Reporting\Documents\" + "211_Data_" + fromDate + "_To_" + toDate + ".csv", csv.ToString());

                serviceResponse.ResponseType = "1";
                serviceResponse.Data = null;
                serviceResponse.RedirectUrl = "";
                serviceResponse.ResponseMessge = "File Downloaded successfully";

                return serviceResponse;

            }
            catch (Exception ex)
            {
                string message = ex.Message;
                throw;
            }


        }

        public static EntityCollection getEntityNew(IOrganizationService service, string startDate, string endDate)
        {
            QueryExpression queryClaims = new QueryExpression()
            {
                EntityName = "incident",
                ColumnSet = new ColumnSet("ccon_mobilephone", "ccon_caseuniqueid", "new_city", "new_country", "new_province", "new_county", "new_postalcode", "ccon_aboriginalstatus", "new_agecategory", "new_agerange", "ccon_disability", "ccon_gender", "ccon_aboriginalstatus", "new_agecategory", "new_agerange", "ccon_disability", "ccon_gender", "ccon_immigrationstatus", "ccon_inquirersmainsourceofincome", "new_interpreterrequired", "ccon_new", "ccon_track", "new_languagespoken", "ccon_aboriginalselfidentified", "ccon_agecategory", "ccon_agerange", "ccon_disabilityselfidentified", "ccon_gender1", "ccon_immigrationstatusselfidentified", "ccon_languagespoken", "ccon_mainsourceofincome", "ccon_newcomer", "ccon_advocacyoffered", "ccon_callnotcompleted", "ccon_contactmethod", "ccon_emergencydisaster", "ccon_followupoffered", "ccon_howdid", "ccon_howyouhearabout211", "ccon_inquiringonbehalfof", "ccon_typeofinquiry", "ccon_warmtransfer", "ccon_warmtransfer", "ccon_axdetails", "ccon_firstname", "ccon_lastname", "new_home", "new_apartment", "new_addressline1", "ccon_workerscomments", "ccon_callersfeedback", "ccon_areyoucomfortableusingtheinternet", "ccon_douhaveaccesstotheinternet", "new_agerange", "ccon_gender", "ccon_howyouhearabout211", "ccon_warmtransfer", "ccon_warmtransferto1"),
                Criteria = new FilterExpression
                {

                    FilterOperator = LogicalOperator.And,
                    Conditions =
                                    {
                                        new ConditionExpression
                                        {
                                            AttributeName = "createdon",
                                            Operator = ConditionOperator.OnOrAfter,
                                            Values = { startDate }
                                        },
                                        new ConditionExpression
                                        {
                                            AttributeName = "createdon",
                                            Operator = ConditionOperator.OnOrBefore,
                                            Values = { endDate }
                                        }
                                    }
                }
            };

            EntityCollection entityCollection = service.RetrieveMultiple(queryClaims);
            return entityCollection;

        }

        public static FetchExpression fethPhoneCall(Guid caseID)
        {
            var fetchXml = $@"<?xml version='1.0'?> <fetch distinct='false'>
                <entity name='phonecall'>
                    <attribute name='ccon_didinquirerfollowuponreferrals'/>
                    <attribute name='ccon_additionalassistanceprovidedifinquirernot'/>
                    <attribute name='ccon_wouldyouuse211again'/>
                    <attribute name='ccon_followupcompletedwithcaller'/>                 
                    <attribute name='ccon_ifyeswhatwastheresult'/>
                    <attribute name='ccon_ifnowhy'/>
                    <attribute name='description'/>
                    <order descending='false' attribute='subject'/>
                        <filter type='and'>
                            <condition attribute='regardingobjectid' operator='eq' value='" + caseID + @"'/>
                        </filter>
                </entity>
            </fetch>";

            return new FetchExpression(fetchXml);
        }


    }
}
