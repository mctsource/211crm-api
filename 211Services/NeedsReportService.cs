﻿using _211Models;
using _211Models.ViewModel;
using GemBox.Spreadsheet;
using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Client;
using Microsoft.Xrm.Sdk.Query;
using Microsoft.Xrm.Tooling.Connector;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using MCT.D365.Package;
using Microsoft.Crm.Sdk.Messages;

namespace _211Services
{
    public class NeedsReportService : CRMDataProvider
    {
        CrmServiceClient _client;
        public NeedsReportService()
        {
            serviceResponse = new ServiceResponse();
            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;

            _client = new CrmServiceClient(ConfigurationManager.AppSettings["CRMConnectionString"]);


            crmSvx = (IOrganizationService)_client.OrganizationWebProxyClient != null ?
                            (IOrganizationService)_client.OrganizationWebProxyClient :

                        (IOrganizationService)_client.OrganizationServiceProxy;

        }


        public ServiceResponse NeedsReport(DownloadCsvModel downloadCsvModel)
        {

            var csv = new StringBuilder();
            
            var heading = string.Format(",DateOfCall,CallReportNum,ReportVersionNum,OrgNum,ResourceAgencyNum,AgencyNamePublic,TaxonomyCode,TaxonomyName,CountryName,StateProvince,CountyName,CityName,PostalCode,AreaCode,PrefixCode,NeedWasUnmet,ReasonIfUnmetOrPartial,Level1Code,Level1Name,Level2Code,Level2Name,Level3Code,Level3Name,Level4Code,Level4Name,Level5Code,Level5Name,PhoneWorkerNum,PhoneWorkerFirstName,PhoneWorkerLastName,AIRSNeedCategory,ReportNeedNum,ParentResourceNum,ParentAgencyName");
            csv.AppendLine(heading);


            try
            {
                string Need = "Need";
                string fromDate = downloadCsvModel.start_date;
                string toDate = downloadCsvModel.end_date;
              
                List<NeedDataModel> listMain = new List<NeedDataModel>();
                List<NeedDataModel> listTaxonomy = new List<NeedDataModel>();
                List<NeedDataModel> listCentralResources = new List<NeedDataModel>();

                FetchExpression fethXml = getNeedEntity1(fromDate, toDate);
                Microsoft.Xrm.Sdk.EntityCollection encryptEntity = _client.RetrieveMultiple(fethXml);

                string[] arrResourceHistoryNumber = new string[encryptEntity.Entities.Count];
                              

                for (int i = 0; i < encryptEntity.Entities.Count; i++)
                {
                    String DateOfCall = "", CallReportNum = "", ReportVersionNum = "", OrgNum = "", ResourceAgencyNum = "", AgencyNamePublic = "", TaxonomyCode = "", TaxonomyName = "", CountryName = "", StateProvince = "", CountyName = "", CityName = "", PostalCode = "", AreaCode = "", PrefixCode = "", NeedWasUnmet = "", ReasonIfUnmetOrPartial = "", Level1Code = "", Level1Name = "", Level2Code = "", Level2Name = "", Level3Code = "", Level3Name = "", Level4Code = "", Level4Name = "", Level5Code = "", Level5Name = "", PhoneWorkerNum = "", PhoneWorkerFirstName = "", PhoneWorkerLastName = "", AIRSNeedCategory = "", ReportNeedNum = "", ParentResourceNum = "", ParentAgencyName = "";

                    NeedDataModel needDataModel = new NeedDataModel();

                    Microsoft.Xrm.Sdk.Entity entity_new = encryptEntity[i];
                    string resourceHistoryID = entity_new.GetAttribute‌​‌​Value<string>("ccon_resourcehistoryid").ToString();
                    arrResourceHistoryNumber[i] = resourceHistoryID;
                    needDataModel.resourceHistoryID = entity_new.GetAttribute‌​‌​Value<string>("ccon_resourcehistoryid").ToString();
                    needDataModel.DateOfCall = " " + entity_new.GetAttribute‌​‌​Value<DateTime>("createdon").ToString("yyyy-MM-dd HH:mm:ss");
                    Boolean needwasmetcheck = entity_new.GetAttribute‌​‌​Value<Boolean>("ccon_referralsmeetingned");

                    needDataModel.NeedWasUnmet = entity_new.FormattedValues["ccon_referralsmeetingned"];

                    needDataModel.ReasonIfUnmetOrPartial = entity_new.GetAttribute‌​‌​Value<string>("ccon_needmetunmetreason");

                    AliasedValue AliasedFirstName = entity_new.GetAttributeValue<AliasedValue>("ao.firstname");
                    if (AliasedFirstName != null)
                        needDataModel.PhoneWorkerFirstName = (AliasedFirstName.Value).ToString();
                    //Primary Contact Lastname
                    AliasedValue AliasedLastName = entity_new.GetAttributeValue<AliasedValue>("ao.lastname");
                    if (AliasedLastName != null)
                        needDataModel.PhoneWorkerLastName = (AliasedLastName.Value).ToString();

                    AliasedValue AliasedCXAgentId = entity_new.GetAttributeValue<AliasedValue>("ao.cdx_agentid");
                    if (AliasedCXAgentId != null)
                        needDataModel.PhoneWorkerNum = (AliasedCXAgentId.Value).ToString();


                    listMain.Add(needDataModel);
                  
                }

                EntityCollection encryptEntityTexonomy = getReferralTaxonomy(_client, arrResourceHistoryNumber);            
                    for (int i = 0; i < encryptEntityTexonomy.Entities.Count; i++)
                    {
                        String Level1Code = "", Level1Name = "", Level2Code = "", Level2Name = "", Level3Code = "", Level3Name = "", Level4Code = "", Level4Name = "", Level5Code = "", Level5Name = "";

                            NeedDataModel needDataModel = new NeedDataModel();

                            needDataModel.resourceHistoryID = encryptEntityTexonomy[i].GetAttributeValue<string>("ccon_programnum");
                            needDataModel.Level1Code = encryptEntityTexonomy[i].GetAttributeValue<string>("ccon_level1code");
                            needDataModel.Level1Name = encryptEntityTexonomy[i].GetAttributeValue<string>("ccon_level1name");
                            needDataModel.Level2Code = encryptEntityTexonomy[i].GetAttributeValue<string>("ccon_level2code");
                            needDataModel.Level2Name = encryptEntityTexonomy[i].GetAttributeValue<string>("ccon_level2name");
                            needDataModel.Level3Code = encryptEntityTexonomy[i].GetAttributeValue<string>("ccon_level3code");
                            needDataModel.Level3Name = encryptEntityTexonomy[i].GetAttributeValue<string>("ccon_level3name");
                            needDataModel.Level4Code = encryptEntityTexonomy[i].GetAttributeValue<string>("ccon_level4code");
                            needDataModel.Level4Name = encryptEntityTexonomy[i].GetAttributeValue<string>("ccon_level4name");
                            needDataModel.Level5Code = encryptEntityTexonomy[i].GetAttributeValue<string>("ccon_level5code");
                            needDataModel.Level5Name = encryptEntityTexonomy[i].GetAttributeValue<string>("ccon_level5name");

                            listTaxonomy.Add(needDataModel);
                    }

                EntityCollection encryptEntityInCentralResoures = getReferralInCentralResoures(_client, arrResourceHistoryNumber);
                for (int i = 0; i < encryptEntityInCentralResoures.Entities.Count; i++)
                {
                    NeedDataModel needDataModel = new NeedDataModel();

                    needDataModel.resourceHistoryID = encryptEntityInCentralResoures[i].GetAttributeValue<String>("ccon_resourceagencynum");
                    needDataModel.AgencyNamePublic = encryptEntityInCentralResoures[i].GetAttributeValue<String>("ccon_siteagencynamepublic");
                    needDataModel.TaxonomyCode = encryptEntityInCentralResoures[i].GetAttributeValue<String>("ccon_taxonomycodes");
                    needDataModel.TaxonomyName = encryptEntityInCentralResoures[i].GetAttributeValue<String>("ccon_taxonomyterm");
                    needDataModel.CountryName = encryptEntityInCentralResoures[i].GetAttributeValue<String>("ccon_mailingcountry");
                    needDataModel.StateProvince = encryptEntityInCentralResoures[i].GetAttributeValue<String>("ccon_mailingstateprovince");
                    needDataModel.CountyName = encryptEntityInCentralResoures[i].GetAttributeValue<String>("ccon_mailingstateprovince");
                    needDataModel.CityName = encryptEntityInCentralResoures[i].GetAttributeValue<String>("ccon_mailingcity");
                    needDataModel.PostalCode = encryptEntityInCentralResoures[i].GetAttributeValue<String>("ccon_mailingpostalcode");
                    needDataModel.AreaCode = encryptEntityInCentralResoures[i].GetAttributeValue<String>("ccon_mailingpostalcode");
                    needDataModel.AIRSNeedCategory = encryptEntityInCentralResoures[i].GetAttributeValue<String>("ccon_programagencynamepublic");

                    listCentralResources.Add(needDataModel);

                }

                for(int i=0;i<listMain.Count;i++)
                {
                    for (int j = 0; j < listTaxonomy.Count; j++)
                    {
                        if(listMain[i].resourceHistoryID == listTaxonomy[j].resourceHistoryID)
                        {

                            listMain[i].Level1Code = listTaxonomy[j].Level1Code;
                            listMain[i].Level1Name = listTaxonomy[j].Level1Name;
                            listMain[i].Level2Code = listTaxonomy[j].Level2Code;
                            listMain[i].Level2Name = listTaxonomy[j].Level2Name;
                            listMain[i].Level3Code = listTaxonomy[j].Level3Code;
                            listMain[i].Level3Name = listTaxonomy[j].Level3Name;
                            listMain[i].Level4Code = listTaxonomy[j].Level4Code;
                            listMain[i].Level4Name = listTaxonomy[j].Level4Name;
                            listMain[i].Level5Code = listTaxonomy[j].Level5Code;
                            listMain[i].Level5Name = listTaxonomy[j].Level5Name;
                        }
                    }

                     for (int k = 0; k < listCentralResources.Count; k++)
                     {
                        if(listMain[i].resourceHistoryID == listCentralResources[k].resourceHistoryID)
                        {

                            listMain[i].resourceHistoryID = listCentralResources[k].resourceHistoryID;
                            listMain[i].AgencyNamePublic = listCentralResources[k].AgencyNamePublic;
                            listMain[i].TaxonomyCode = listCentralResources[k].TaxonomyCode;
                            listMain[i].TaxonomyName = listCentralResources[k].TaxonomyName;
                            listMain[i].CountryName = listCentralResources[k].CountryName;
                            listMain[i].StateProvince = listCentralResources[k].StateProvince;
                            listMain[i].CountyName = listCentralResources[k].CountyName;
                            listMain[i].CityName = listCentralResources[k].CityName;
                            listMain[i].PostalCode = listCentralResources[k].PostalCode;
                            listMain[i].AreaCode = listCentralResources[k].AreaCode;
                            listMain[i].AIRSNeedCategory = listCentralResources[k].AIRSNeedCategory; 
                        }
                    }

                    var newLine = string.Format("{0},{1},{2},{3},{4},{5},{6},{7},{8},{9},{10},{11},{12},{13},{14},{15},{16},{17},{18},{19},{20},{21},{22},{23},{24},{25},{26},{27},{28},{29},{30},{31},{32},{33},{34}", string.Empty, listMain[i].DateOfCall, listMain[i].CallReportNum, listMain[i].ReportVersionNum, listMain[i].OrgNum, listMain[i].ResourceAgencyNum, listMain[i].AgencyNamePublic, listMain[i].TaxonomyCode, listMain[i].TaxonomyName, listMain[i].CountryName, listMain[i].StateProvince, listMain[i].CountyName, listMain[i].CityName, listMain[i].PostalCode, listMain[i].AreaCode, listMain[i].PrefixCode, listMain[i].NeedWasUnmet, listMain[i].ReasonIfUnmetOrPartial, listMain[i].Level1Code, listMain[i].Level1Name, listMain[i].Level2Code, listMain[i].Level2Name, listMain[i].Level3Code, listMain[i].Level3Name, listMain[i].Level4Code, listMain[i].Level4Name, listMain[i].Level5Code, listMain[i].Level5Name, listMain[i].PhoneWorkerNum, listMain[i].PhoneWorkerFirstName, listMain[i].PhoneWorkerLastName, listMain[i].AIRSNeedCategory, listMain[i].ReportNeedNum, listMain[i].ParentResourceNum, listMain[i].ParentAgencyName);

                     csv.AppendLine(newLine);
                }

                HttpContext.Current.Response.Clear();
                HttpContext.Current.Response.Buffer = true;
                HttpContext.Current.Response.AddHeader("content-disposition", "attachment;filename= 211_NeedReport_" + fromDate + "_To_" + toDate + ".csv");
                HttpContext.Current.Response.Charset = "";
                HttpContext.Current.Response.ContentType = "text/csv";
                HttpContext.Current.Response.Output.Write(csv);
                HttpContext.Current.Response.Flush();
                HttpContext.Current.Response.End();

                serviceResponse.ResponseType = "1";
                serviceResponse.Data = null;
                serviceResponse.RedirectUrl = "";
                serviceResponse.ResponseMessge = "File Downloaded successfully";

                return serviceResponse;

            }
            catch (Exception ex)
            {
                string message = ex.Message;
                throw;
            }



        }     
        public static FetchExpression getNeedEntity1(string startDate, string endDate)
        {
            var fetchXml = $@"<fetch version='1.0' output-format='xml-platform' mapping='logical' distinct='false'>
                              <entity name='ccon_need'>
                                <attribute name='ccon_needid' />
                                <attribute name='ccon_name' />
                                <attribute name='createdon' />                               
                                <attribute name='ccon_resourcehistoryid' />
                                <attribute name = 'ccon_referralsmeetingned' />
                                <attribute name='ccon_needmetunmetreason' />
                                <attribute name = 'ccon_case' />
                                <order attribute='ccon_name' descending='false' />
                                <filter type='and'>
                                  <condition attribute='createdon' operator='on-or-after' value= '{ startDate }' />
                                  <condition attribute='createdon' operator='on-or-before' value= '{ endDate }' />
                                </filter>
                                <link-entity name='incident' from='incidentid' to='ccon_case' link-type='inner' alias='am'>
                                  <link-entity name='ccon_axagentdetails' from='ccon_axagentdetailsid' to='ccon_axdetails' link-type='inner' alias='an'>
                                    <link-entity name='systemuser' from='systemuserid' to='createdby' link-type='inner' alias='ao'>
                                      <attribute name='firstname'/>  
                                      <attribute name='lastname' />                                   
                                      <attribute name='cdx_agentid' />                                   
                                    </link-entity>
                                  </link-entity>
                                </link-entity>
                              </entity>
                            </fetch>";
            return new FetchExpression(fetchXml);
        }

        public static Microsoft.Xrm.Sdk.EntityCollection getReferralInCentralResoures(IOrganizationService service, string[] arrResourceHistoryNumber1)
        {
            string[] arr = new string[arrResourceHistoryNumber1.Count()];
            for (int i = 0; i < arrResourceHistoryNumber1.Count(); i++)
            {
                arr[i] = arrResourceHistoryNumber1[i];
            }           
            QueryExpression query = new QueryExpression("ccon_centralresources");
            query.ColumnSet.AddColumns("ccon_centralresourcesid", "ccon_resourceagencynum", "createdon", "ccon_siteagencynamepublic", "ccon_taxonomycodes", "ccon_taxonomyterm", "ccon_mailingcountry", "ccon_mailingstateprovince", "ccon_mailingcity", "ccon_programagencynamepublic");
            FilterExpression childFilter = query.Criteria.AddFilter(LogicalOperator.And);
            childFilter.AddCondition("ccon_resourceagencynum", ConditionOperator.In, arr);

            Microsoft.Xrm.Sdk.EntityCollection entityCollection = service.RetrieveMultiple(query);
            return entityCollection;

        }

        public static Microsoft.Xrm.Sdk.EntityCollection getReferralTaxonomy(IOrganizationService service, string[] arrResourceHistoryNumber1)
        {
            string[] arr = new string[arrResourceHistoryNumber1.Count()];
            for (int i = 0; i < arrResourceHistoryNumber1.Count(); i++)
            {
                arr[i] = arrResourceHistoryNumber1[i];
            }
            
            QueryExpression query = new QueryExpression("ccon_centralresourcesintaxonomy");
            query.ColumnSet.AddColumns("ccon_level1code", "ccon_programnum", "ccon_level1name", "ccon_level2code", "ccon_level2name", "ccon_level3code", "ccon_level3name", "ccon_level4code", "ccon_level4name", "ccon_level5code", "ccon_level5name");
            FilterExpression childFilter = query.Criteria.AddFilter(LogicalOperator.And);
            childFilter.AddCondition("ccon_programnum", ConditionOperator.In, arr);

            Microsoft.Xrm.Sdk.EntityCollection entityCollection = service.RetrieveMultiple(query);
            return entityCollection;

        }

    }
}
