﻿using MongoDB.Bson;
using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace _211Services
{
    public static class Common
    {
        static string erroLogConnectionString = ConfigurationManager.AppSettings["ErrorLogConnection"].ToString();
        // static string MongoConnectionString = @"AuthType=Office365;Username=msp@mctit1110.onmicrosoft.com;Password=Ravi@123456789;URL=https://phss.crm8.dynamics.com/;";
        static MongoClient Client = null;
        static BsonDocument loggingBson = null;
        /// <summary>
        /// Used for generating random strings like password
        /// </summary>
        /// <param name="size"></param>
        /// <param name="lowerCase"></param>
        /// <returns></returns>
           public static string RandomString(int size, bool lowerCase)
           {
            StringBuilder builder = new StringBuilder();
        Random random = new Random();
        char ch;
           for (int i = 0; i<size; i++)
           {
                ch = Convert.ToChar(Convert.ToInt32(Math.Floor(26 * random.NextDouble() + 65)));
                builder.Append(ch);
           }
           if (lowerCase)
                return builder.ToString().ToLower();
            return builder.ToString();
           }
        ///Used For Getting file Base64 from Azure Blob
        //public static string FromAzureToBase64(string azureUri)
        //{
        //    Uri blobUri = new Uri(azureUri);
        //    StorageCredentials creden = new StorageCredentials("", "");
        //    CloudBlockBlob blob = new CloudBlockBlob(blobUri, creden);
        //    blob.FetchAttributes();//Fetch blob's properties
        //    byte[] arr = new byte[blob.Properties.Length];
        //    blob.DownloadToByteArray(arr, 0);
        //    var azureBase64 = Convert.ToBase64String(arr);
        //    return azureBase64;
        //}
        public static DateTime ConvertDateTimeToEst(DateTime timeUtc)
        {
            TimeZoneInfo easternZone = TimeZoneInfo.FindSystemTimeZoneById("Eastern Standard Time");
            DateTime easternTime = TimeZoneInfo.ConvertTime(timeUtc, easternZone);
            return easternTime;
        }
        public static IQueryable<T> OrderByDynamic<T>(this IQueryable<T> query, string sortColumn, bool descending)
        {
            // Dynamically creates a call like this: query.OrderBy(p =&gt; p.SortColumn)
            var parameter = Expression.Parameter(typeof(T), "p");

            string command = "OrderBy";

            if (descending)
            {
                command = "OrderByDescending";
            }

            Expression resultExpression = null;

            var property = typeof(T).GetProperty(sortColumn);
            // this is the part p.SortColumn
            var propertyAccess = Expression.MakeMemberAccess(parameter, property);

            // this is the part p =&gt; p.SortColumn
            var orderByExpression = Expression.Lambda(propertyAccess, parameter);

            // finally, call the "OrderBy" / "OrderByDescending" method with the order by lamba expression
            resultExpression = Expression.Call(typeof(Queryable), command, new Type[] { typeof(T), property.PropertyType },
               query.Expression, Expression.Quote(orderByExpression));

            return query.Provider.CreateQuery<T>(resultExpression);
        }

        public static string GetChangeBy(string userName)
        {
            if (!string.IsNullOrWhiteSpace(userName))
            {
                return userName.Replace(" ", "_") + "_" + DateTime.Now.Ticks.ToString();
            }
            else
            {
                return "Test_" + DateTime.Now.Ticks.ToString();
            }

        }
        public static DataTable ToDataTable<T>(List<T> items)
        {
            DataTable dataTable = new DataTable(typeof(T).Name);

            //Get all the properties
            PropertyInfo[] Props = typeof(T).GetProperties();
            foreach (PropertyInfo prop in Props)
            {
                //Defining type of data column gives proper data table 
                var type = (prop.PropertyType.IsGenericType && prop.PropertyType.GetGenericTypeDefinition() == typeof(Nullable<>) ? Nullable.GetUnderlyingType(prop.PropertyType) : prop.PropertyType);

                dataTable.Columns.Add(prop.Name, type);
            }
            foreach (T item in items)
            {
                var values = new object[Props.Length];
                for (int i = 0; i < Props.Length; i++)
                {
                    //inserting property values to datatable rows
                    values[i] = Props[i].GetValue(item, null);
                }
                dataTable.Rows.Add(values);
            }
            //put a breakpoint here and check datatable
            return dataTable;
        }
        public static void ErrorLogToMongoDB(Exception ex)
        {
            Client = new MongoClient(erroLogConnectionString);
            IMongoDatabase DB = Client.GetDatabase("PHSSDB");
            var st = new StackTrace(ex, true);
            var document = new BsonDocument {
                        {"DateTime",DateTime.UtcNow},
                        {"ErrorMessage",ex.Message},
                        {"LineNumber",st.GetFrame(0).GetFileLineNumber()},
                };
            var collection = DB.GetCollection<BsonDocument>("ErrorLog");
            collection.InsertOne(document);
        }
        public static bool IsBetweenInTime<T>(this T item, T start, T end)
        {
            return (Comparer<T>.Default.Compare(item, start) >= 0
                && Comparer<T>.Default.Compare(item, end) <= 0);
        }
        public static bool IsBetweenOutTime<T>(this T item, T start, T end)
        {
            return (Comparer<T>.Default.Compare(item, start) <= 0
                && Comparer<T>.Default.Compare(item, end) >= 0);
        }
        public static string ConvertHoursInDecimalPhss(string hhMMString)
        {
            if (hhMMString.Split(':')[1] == "15")
            {
                hhMMString = hhMMString.Split(':')[0] + ":25";
            }
            else if (hhMMString.Split(':')[1] == "30")
            {
                hhMMString = hhMMString.Split(':')[0] + ":50";
            }
            else if (hhMMString.Split(':')[1] == "45")
            {
                hhMMString = hhMMString.Split(':')[0] + ":75";
            }
            return hhMMString.Replace(':', '.');
        }
    }
}
