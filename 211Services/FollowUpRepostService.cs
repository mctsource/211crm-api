﻿using _211Models;
using _211Models.ViewModel;
using GemBox.Spreadsheet;
using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Query;
using Microsoft.Xrm.Tooling.Connector;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace _211Services
{
    public class FollowUpRepostService:CRMDataProvider
    {
        CrmServiceClient _client;
        public FollowUpRepostService()
        {
            serviceResponse = new ServiceResponse();
            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;

            _client = new CrmServiceClient(ConfigurationManager.AppSettings["CRMConnectionString"]);


            crmSvx = (IOrganizationService)_client.OrganizationWebProxyClient != null ?
                            (IOrganizationService)_client.OrganizationWebProxyClient :

                        (IOrganizationService)_client.OrganizationServiceProxy;

        }


        public ServiceResponse FollowupReport(DownloadCsvModel downloadCsvModel)
        {

            var csv = new StringBuilder();
            var heading = string.Format(",CallReportNum,ReportVersion,DateOfInitialCall,CallerName,PersonEnteringActivity,EnteredOn,UpdatedOn,ActivityType,ActivityStatus,PurposeIsAdvocacy,PurposeIsAssistance,PurposeIsSurvey,SurveyDeclinedReason,ActvityTitle,ActivityMessage,DurationInMinutes,DueOn,TargetPhoneNumber,TargetEmailSMS,NameToAskFor,OKToLeaveMessage,NumberOfSuccessfulAttemptsMade,ContactAttemptsMadeSoFar,AssignedToWorkerNum,AssignedToWorkerName,OrgNum,ReportVersionNum");
            csv.AppendLine(heading);

            try
            {

               

                string fromDate = downloadCsvModel.start_date;
                string toDate = downloadCsvModel.end_date;
               // EntityCollection encryptEntity = getScheduleEntity(_client, fromDate, toDate);

                FetchExpression fethXml = getFollowupEntity(fromDate, toDate);
                Microsoft.Xrm.Sdk.EntityCollection encryptEntity = _client.RetrieveMultiple(fethXml);


                for (int i = 0; i < encryptEntity.Entities.Count; i++)
                {

                    String CallReportNum = string.Empty, ReportVersion = string.Empty,
                        DateOfInitialCall = string.Empty, CallerName = string.Empty, PersonEnteringActivity = string.Empty, EnteredOn = string.Empty, UpdatedOn = string.Empty, ActivityType = "", ActivityStatus = "", PurposeIsAdvocacy = "", PurposeIsAssistance = "", PurposeIsSurvey = "", SurveyDeclinedReason = string.Empty, ActvityTitle = string.Empty, ActivityMessage = string.Empty, DurationInMinutes = string.Empty, DueOn = string.Empty, TargetPhoneNumber = string.Empty, TargetEmailSMS = string.Empty, NameToAskFor = string.Empty, OKToLeaveMessage = string.Empty, NumberOfContactAttemptsToMake = string.Empty, ContactAttemptsMadeSoFar = string.Empty, AssignedToWorkerNum = string.Empty, AssignedToWorkerName = string.Empty, OrgNum = string.Empty, ReportVersionNum = string.Empty;


                    Microsoft.Xrm.Sdk.Entity entity_new = encryptEntity[i];
                    ActvityTitle = entity_new.GetAttribute‌​‌​Value<string>("subject");
                    ActivityMessage = entity_new.GetAttribute‌​‌​Value<string>("description");
                    if (entity_new.GetAttribute‌​‌​Value<DateTime>("scheduledend") != null) 
                        DueOn = " " + entity_new.GetAttribute‌​‌​Value<DateTime>("scheduledend").ToString("yyyy-MM-dd HH:mm:ss");
                    NameToAskFor = entity_new.GetAttribute‌​‌​Value<string>("ccon_nametoaskfor");
                    if (entity_new.GetAttribute‌​‌​Value<OptionSetValue>("ccon_phonecallitisoktoleavethemessage") != null)
                        OKToLeaveMessage = entity_new.FormattedValues["ccon_phonecallitisoktoleavethemessage"];
                    if (entity_new.GetAttribute‌​‌​Value<OptionSetValue>("ccon_trytocontact1") != null)
                        ContactAttemptsMadeSoFar = entity_new.FormattedValues["ccon_trytocontact1"];

                    if (entity_new.GetAttribute‌​‌​Value<OptionSetValue>("ccon_numberofsuccessfulattemptsmade") != null)
                        NumberOfContactAttemptsToMake = entity_new.FormattedValues["ccon_numberofsuccessfulattemptsmade"];

                    TargetPhoneNumber = entity_new.GetAttribute‌​‌​Value<string>("ccon_contactnumber");
                    TargetEmailSMS = entity_new.GetAttribute‌​‌​Value<string>("ccon_email1");

                    AliasedValue AliasedCallerName = entity_new.GetAttributeValue<AliasedValue>("aj.customerid");
                    if (AliasedCallerName != null)
                    {
                        EntityReference callerEntity = ((EntityReference)AliasedCallerName.Value);

                        CallerName = callerEntity.Name;
                    }
                        
                   
                    ActivityStatus = entity_new.FormattedValues["aj.statuscode"];

                    /* CallerName = caselEntity.Attributes.Contains("customerid") ? (caselEntity.GetAttribute‌​‌​Value<EntityReference>("customerid")).Name : "";
                     ActivityStatus = caselEntity.Attributes.Contains("statuscode") ? (caselEntity.FormattedValues["statuscode"]) : ""; */

                   // CallReportNum = axDetailEntity.GetAttribute‌​‌​Value<string>("ccon_incontactid");
                    AliasedValue AliasedCallReportNum = entity_new.GetAttributeValue<AliasedValue>("ak.ccon_incontactid");
                    if (AliasedCallReportNum != null)
                        CallReportNum = (AliasedCallReportNum.Value).ToString();

                    // DateOfInitialCall = " " + axDetailEntity.GetAttribute‌​‌​Value<DateTime>("ccon_startime").ToString("yyyy-MM-dd HH:mm:ss");
                    AliasedValue AliasedDateOfInitialCall = entity_new.GetAttributeValue<AliasedValue>("ak.ccon_startime");
                    if (AliasedDateOfInitialCall != null)
                    {
                        DateTime DateOfInitialCallDate = ((DateTime)AliasedDateOfInitialCall.Value);
                        DateOfInitialCall = DateOfInitialCallDate.ToString("yyyy-MM-dd HH:mm:ss");
                    }
                        
                    //EnteredOn = " " + axDetailEntity.GetAttribute‌​‌​Value<DateTime>("ccon_startime").ToString("yyyy-MM-dd HH:mm:ss");
                    AliasedValue AliasedEnteredOn = entity_new.GetAttributeValue<AliasedValue>("ak.ccon_startime");
                    if (AliasedEnteredOn != null)
                    {
                        DateTime DateEnteredOn = ((DateTime)AliasedEnteredOn.Value);
                        EnteredOn = DateEnteredOn.ToString("yyyy-MM-dd HH:mm:ss");
                    }

                   // DurationInMinutes = axDetailEntity.Attributes.Contains("ccon_length") ? (axDetailEntity.GetAttribute‌​‌​Value<int>("ccon_length")).ToString() : string.Empty;
                    AliasedValue AliasedDurationInMinutes = entity_new.GetAttributeValue<AliasedValue>("ak.ccon_length");
                    if (AliasedDurationInMinutes != null)
                        DurationInMinutes = (AliasedDurationInMinutes.Value).ToString();

                    //ActivityType = axDetailEntity.Attributes.Contains("ccon_campaignid") ? (axDetailEntity.GetAttribute‌​‌​Value<EntityReference>("ccon_campaignid")).Name : "";
                    AliasedValue AliasedActivityType = entity_new.GetAttributeValue<AliasedValue>("ak.ccon_campaignid");
                    if (AliasedActivityType != null)
                    {
                        EntityReference DurationInMinutesRefence = ((EntityReference)AliasedActivityType.Value);
                        ActivityType = DurationInMinutesRefence.Name;
                    }
                        

                    String firstnameN = "", lastnameN = "";
                    AliasedValue AliasedFirstName = entity_new.GetAttributeValue<AliasedValue>("al.firstname");
                    if (AliasedFirstName != null)
                        firstnameN = (AliasedFirstName.Value).ToString();
                    //Primary Contact Lastname
                    AliasedValue AliasedLastName = entity_new.GetAttributeValue<AliasedValue>("al.lastname");
                    if (AliasedLastName != null)
                        lastnameN = (AliasedLastName.Value).ToString();

                    PersonEnteringActivity = firstnameN + lastnameN;


                   /* Guid caseGuid = entity_new.Attributes.Contains("regardingobjectid") ? (entity_new.GetAttribute‌​‌​Value<EntityReference>("regardingobjectid")).Id : Guid.Empty;
                    if (caseGuid != Guid.Empty)
                    {
                        // CallReportNum = "Report2";
                        Microsoft.Xrm.Sdk.Entity caselEntity = _client.Retrieve("incident", caseGuid, new ColumnSet("ccon_axdetails", "customerid", "statuscode"));                       

                        Guid cxDetailGuid = caselEntity.Attributes.Contains("ccon_axdetails") ? (caselEntity.GetAttribute‌​‌​Value<EntityReference>("ccon_axdetails")).Id : Guid.Empty;
                        if (cxDetailGuid != Guid.Empty)
                        {
                            Microsoft.Xrm.Sdk.Entity axDetailEntity = _client.Retrieve("ccon_axagentdetails", cxDetailGuid, new ColumnSet("ccon_incontactid", "ccon_startime", "ccon_contactnumber", "createdby", "ccon_campaignid", "ccon_length"));
                           
                            if (axDetailEntity.GetAttribute‌​‌​Value<DateTime>("ccon_startime") != null)
                               
                            if (axDetailEntity.GetAttribute‌​‌​Value<DateTime>("ccon_startime") != null)
                              

                           


                            Guid workerGuid = axDetailEntity.Attributes.Contains("createdby") ? (axDetailEntity.GetAttribute‌​‌​Value<EntityReference>("createdby")).Id : Guid.Empty;
                            if (workerGuid != Guid.Empty)
                            {
                                Microsoft.Xrm.Sdk.Entity workerEntity = _client.Retrieve("systemuser", workerGuid, new ColumnSet("firstname", "lastname"));
                                PersonEnteringActivity = workerEntity.GetAttribute‌​‌​Value<string>("firstname") + " " + workerEntity.GetAttribute‌​‌​Value<string>("lastname");

                            }

                            

                        }


                    } */

                    var newLine = string.Format("{0},{1},{2},{3},{4},{5},{6},{7},{8},{9},{10},{11},{12},{13},{14},{15},{16},{17},{18},{19},{20},{21},{22},{23},{24},{25},{26},{27}", string.Empty, CallReportNum, ReportVersion, DateOfInitialCall, CallerName, PersonEnteringActivity, EnteredOn, UpdatedOn, ActivityType, ActivityStatus, PurposeIsAdvocacy, PurposeIsAssistance, PurposeIsSurvey, SurveyDeclinedReason, ActvityTitle, ActivityMessage, DurationInMinutes, DueOn, TargetPhoneNumber, TargetEmailSMS, NameToAskFor, OKToLeaveMessage, NumberOfContactAttemptsToMake, ContactAttemptsMadeSoFar, AssignedToWorkerNum, AssignedToWorkerName, OrgNum, ReportVersionNum);

                    csv.AppendLine(newLine);
                }



                HttpContext.Current.Response.Clear();
                HttpContext.Current.Response.Buffer = true;
                HttpContext.Current.Response.AddHeader("content-disposition", "attachment;filename=211_FollowUpReport_" + fromDate + "_To_" + toDate + ".csv");
                HttpContext.Current.Response.Charset = "";
                HttpContext.Current.Response.ContentType = "text/csv";
                HttpContext.Current.Response.Output.Write(csv);
                HttpContext.Current.Response.Flush();
                HttpContext.Current.Response.End();

                serviceResponse.ResponseType = "1";
                serviceResponse.Data = null;
                serviceResponse.RedirectUrl = "";
                serviceResponse.ResponseMessge = "File Downloaded successfully";

                return serviceResponse;

            }
            catch (Exception ex)
            {
                string message = ex.Message;
                throw;
            }


        }
        public static EntityCollection getScheduleEntity(IOrganizationService service, string startDate, string endDate)
        {
            QueryExpression queryClaims = new QueryExpression()
            {
                EntityName = "phonecall",
                ColumnSet = new ColumnSet("createdon", "subject", "description", "scheduledend", "ccon_nametoaskfor", "ccon_phonecallitisoktoleavethemessage", "ccon_trytocontact1", "ownerid", "regardingobjectid", "ccon_contactnumber", "ccon_email1", "ccon_numberofsuccessfulattemptsmade"),
                Criteria = new FilterExpression
                {

                    FilterOperator = LogicalOperator.And,
                    Conditions =
                                    {
                                        new ConditionExpression
                                        {
                                            AttributeName = "createdon",
                                            Operator = ConditionOperator.OnOrAfter,
                                            Values = { startDate }
                                        },
                                        new ConditionExpression
                                        {
                                            AttributeName = "createdon",
                                            Operator = ConditionOperator.OnOrBefore,
                                            Values = { endDate }
                                        }
                                    }
                }
            };

            EntityCollection entityCollection = service.RetrieveMultiple(queryClaims);
            return entityCollection;

        }

        public static FetchExpression getFollowupEntity(string startDate, string endDate)
        {            
            var fetchXml = $@"<fetch version='1.0' output-format='xml-platform' mapping='logical' distinct='false'>
                  <entity name='phonecall'>
                    <attribute name='subject' />
                    <attribute name='statecode' />
                    <attribute name='prioritycode' />
                    <attribute name='scheduledend' />
                    <attribute name='createdby' />
                    <attribute name='regardingobjectid' />
                    <attribute name='activityid' />
                    <attribute name='createdon' />
                    <attribute name='description' />
                    <attribute name='ccon_nametoaskfor' />
                    <attribute name='ccon_phonecallitisoktoleavethemessage' />
                    <attribute name='ccon_trytocontact1' />
                    <attribute name='ownerid' />
                    <attribute name='ccon_email1' />
                    <attribute name='ccon_contactnumber' />
                    <attribute name='ccon_numberofsuccessfulattemptsmade' />
                    <order attribute='subject' descending='false' />
                    <filter type='and'>
                      <condition attribute='createdon' operator='on-or-after' value= '{ startDate }' />
                      <condition attribute='createdon' operator='on-or-before' value= '{ endDate }'/>
                    </filter>
                    <link-entity name='incident' from='incidentid' to='regardingobjectid' link-type='inner' alias='aj'>
                      <attribute name= 'ccon_axdetails' />
                      <attribute name= 'customerid' />
                      <attribute name= 'statuscode' />
                      <link-entity name='ccon_axagentdetails' from='ccon_axagentdetailsid' to='ccon_axdetails' link-type='inner' alias='ak'>
                         <attribute name= 'ccon_incontactid' />
                         <attribute name= 'ccon_startime' />
                         <attribute name= 'ccon_contactnumber' />
                         <attribute name= 'createdby' />
                         <attribute name= 'ccon_campaignid' />
                         <attribute name= 'ccon_length' />
                        <link-entity name='systemuser' from='systemuserid' to='createdby' link-type='inner' alias='al' >
                            <attribute name= 'firstname' />
                            <attribute name= 'lastname' />
                        </link-entity>
                      </link-entity>
                    </link-entity>
                  </entity>
                </fetch>";
            return new FetchExpression(fetchXml);
        }

    }
}
