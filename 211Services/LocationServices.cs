﻿using _211Models;
using _211Models.PostModel;
using _211Models.ViewModel;
using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Query;
using Microsoft.Xrm.Tooling.Connector;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace _211Services
{
    public class LocationServices : CRMDataProvider
    {
        public LocationServices()
        {

            serviceResponse = new ServiceResponse();
            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;

            CrmServiceClient crmSvccon = new CrmServiceClient(ConfigurationManager.AppSettings["CRMConnectionString"]);


            crmSvx = (IOrganizationService)crmSvccon.OrganizationWebProxyClient != null ?
                        (IOrganizationService)crmSvccon.OrganizationWebProxyClient :
                        (IOrganizationService)crmSvccon.OrganizationServiceProxy;
        }
      

        public ServiceResponse LocationHierarchy(LocationHierarchyModel locationHierarchyModel)
       {
            
            LocationView locationdetail = new LocationView();
            List<LocationView> locationlist = new List<LocationView>();
            ProvinceView provinceView = new ProvinceView();
            CountiesView CountyView= new CountiesView();
            List<CountiesView> CountiesList = new List<CountiesView>();
            List<ProvinceView> provinceslist = new List<ProvinceView>();
            CityView cityview = new CityView();
            List<CityView> citylist = new List<CityView>();

            if (locationHierarchyModel.HierarchyId == "1")
            {
                try
                {
                    EntityCollection country = new EntityCollection();

                    QueryExpression countryquery = new QueryExpression()
                    {

                        EntityName = "ccon_mylocation",
                        ColumnSet = new ColumnSet(true),
                        Criteria = new FilterExpression
                        {
                            FilterOperator = LogicalOperator.And,
                            Conditions =
                            {
                                new ConditionExpression
                                {
                                AttributeName = "ccon_type",
                                Operator = ConditionOperator.Equal,
                                Values = { 757130000 }
                                }
                            }
                        }

                    };
                    country = crmSvx.RetrieveMultiple(countryquery);
                    if (country.Entities.Count > 0)
                    {
                        for (int i = 0; i < country.Entities.Count(); i++)
                        {
                            EntityCollection countryParent = new EntityCollection();


                            QueryExpression countryparentquery = new QueryExpression()
                            {

                                EntityName = "ccon_mylocation",
                                ColumnSet = new ColumnSet(true),
                                Criteria = new FilterExpression
                                {
                                    FilterOperator = LogicalOperator.And,
                                    Conditions =
                    {
                        new ConditionExpression
                        {
                        AttributeName = "ccon_parentlocation",
                        Operator = ConditionOperator.Equal,
                        Values = { country.Entities[i].Id.ToString() }
                        }
                    }
                                }
                            };
                            countryParent = crmSvx.RetrieveMultiple(countryparentquery);
                            if (countryParent.Entities.Count > 0)
                            {

                                locationdetail = new LocationView();
                                locationdetail.ChildType = countryParent.Entities[i].Contains("ccon_type") ? countryParent.Entities[i].FormattedValues["ccon_type"].ToString() : "null";
                                locationdetail.CountryName = country.Entities[i].Attributes["ccon_name"].ToString();
                                locationdetail.CountryGUID = country.Entities[i].Id.ToString();
                                locationdetail.ParentLocation = "null";
                                locationlist.Add(locationdetail);

                            }
                            else
                            {
                                serviceResponse.ResponseType = "2";
                                serviceResponse.Data = null;

                                serviceResponse.ResponseMessge = "Data Not Found";
                            }
                        }
                    }

                }
                catch (Exception ex)
                {
                    serviceResponse.ResponseType = "3";
                    serviceResponse.Data = null;
                    serviceResponse.ResponseMessge = ResponseMessage.RequestFailed;
                    return serviceResponse;
                }
                if (locationlist != null)
                {
                    serviceResponse.ResponseType = "1";
                    serviceResponse.Data = locationlist.OrderBy(x => x.CountryName);

                    serviceResponse.ResponseMessge = "Data Found Successfully";
                }
                else
                {
                    serviceResponse.ResponseType = "2";
                    serviceResponse.Data = null;

                    serviceResponse.ResponseMessge = "Data Not Found";
                }
            }
            else if(locationHierarchyModel.HierarchyId == "2")
            {
                try
                {
                    EntityCollection province = new EntityCollection();
                    QueryExpression provincequery = new QueryExpression()
                    {

                        EntityName = "ccon_mylocation",
                        ColumnSet = new ColumnSet(true),
                        Criteria = new FilterExpression
                        {
                            FilterOperator = LogicalOperator.And,
                            Conditions =
                        {
                            new ConditionExpression
                            {
                                AttributeName = "ccon_type",
                                Operator = ConditionOperator.Equal,
                                Values = { 757130001 }
                            },
                            new ConditionExpression
                            {
                                AttributeName = "ccon_parentlocation",
                                Operator = ConditionOperator.Equal,
                                Values = { locationHierarchyModel.GUID }
                            }
                        }
                        }
                    };
                    province = crmSvx.RetrieveMultiple(provincequery);
                    if (province.Entities.Count() > 0)
                    {
                        for (int i = 0; i < province.Entities.Count(); i++)
                        {
                                    provinceView = new ProvinceView();
                                    provinceView.ProvinceGuid = province.Entities[i].Id.ToString();
                                    provinceView.ProvinceName = province.Entities[i].Attributes["ccon_name"].ToString();
                                    provinceView.parentLocation = province.Entities[i].Contains("ccon_parentlocation") ? province.Entities[i].GetAttributeValue<EntityReference>("ccon_parentlocation").Name.ToString(): "null";
                                     provinceView.ProvinceCode = province.Entities[i].Contains("ccon_provincecode") ? province.Entities[i].Attributes["ccon_provincecode"].ToString() : "null";


                            //provinceView.ChildType = provincechild.Entities[j].Contains("ccon_type") ? provincechild.Entities[j].FormattedValues["ccon_type"].ToString() : "null"; ;

                            EntityCollection provincechild = new EntityCollection();
                            QueryExpression provincechildquery = new QueryExpression()
                            {

                                EntityName = "ccon_mylocation",
                                ColumnSet = new ColumnSet(true),
                                Criteria = new FilterExpression
                                {
                                    FilterOperator = LogicalOperator.And,
                                    Conditions =
                        {
                            new ConditionExpression
                            {
                                AttributeName = "ccon_parentlocation",
                                Operator = ConditionOperator.Equal,
                                Values = {  provinceView.ProvinceGuid  }
                            }
                        }
                                }
                            };
                            provincechild = crmSvx.RetrieveMultiple(provincechildquery);
                            if (provincechild.Entities.Count() > 0)
                            {
                                for(int j = 0; j < 1; j++)
                                provinceView.ChildType = provincechild.Entities[j].Contains("ccon_type") ? provincechild.Entities[j].FormattedValues["ccon_type"].ToString() : "null"; 
                            }
                               
                            provinceslist.Add(provinceView);
                        }
                        if (provinceslist.Count>0)
                        {
                            serviceResponse.ResponseType = "1";
                            serviceResponse.Data = provinceslist.OrderBy(x => x.ProvinceName);
                            serviceResponse.ResponseMessge = "Data Found Successfully";
                        }
                        else
                        {
                            serviceResponse.ResponseType = "2";
                            serviceResponse.Data = null;
                            serviceResponse.ResponseMessge = "Data Not Found";
                        }

                    }
                }
                catch (Exception ex)
                {
                    serviceResponse.ResponseType = "3";
                    serviceResponse.Data = null;
                    serviceResponse.ResponseMessge = ResponseMessage.RequestFailed;
                }
               
            }
            else if (locationHierarchyModel.HierarchyId == "3")
            {
                try
                {
                    EntityCollection County = new EntityCollection();
                    QueryExpression Countyquery = new QueryExpression()
                    {

                        EntityName = "ccon_mylocation",
                        ColumnSet = new ColumnSet(true),
                        Criteria = new FilterExpression
                        {
                            FilterOperator = LogicalOperator.And,
                            Conditions =
                        {
                            new ConditionExpression
                            {
                                AttributeName = "ccon_type",
                                Operator = ConditionOperator.Equal,
                                Values = { 757130002 }
                            },
                            new ConditionExpression
                            {
                                AttributeName = "ccon_parentlocation",
                                Operator = ConditionOperator.Equal,
                                Values = { locationHierarchyModel.GUID }
                            }
                        }
                        }
                    };
                    County = crmSvx.RetrieveMultiple(Countyquery);
                    if (County.Entities.Count() > 0)
                    {
                        for (int i = 0; i < County.Entities.Count(); i++)
                        {
                            CountyView = new CountiesView();
                            CountyView.CountyGUID = County.Entities[i].Id.ToString();
                            CountyView.CountyName = County.Entities[i].Attributes["ccon_name"].ToString();
                            CountyView.ParentLocation = County.Entities[i].Contains("ccon_parentlocation") ? County.Entities[i].GetAttributeValue<EntityReference>("ccon_parentlocation").Name.ToString() : "null";

                            //provinceView.ChildType = provincechild.Entities[j].Contains("ccon_type") ? provincechild.Entities[j].FormattedValues["ccon_type"].ToString() : "null"; ;

                            EntityCollection countychild = new EntityCollection();
                            QueryExpression countychildquery = new QueryExpression()
                            {

                                EntityName = "ccon_mylocation",
                                ColumnSet = new ColumnSet(true),
                                Criteria = new FilterExpression
                                {
                                    FilterOperator = LogicalOperator.And,
                                    Conditions =
                        {
                            new ConditionExpression
                            {
                                AttributeName = "ccon_parentlocation",
                                Operator = ConditionOperator.Equal,
                                Values = {   CountyView.CountyGUID  }
                            }
                        }
                                }
                            };
                            countychild = crmSvx.RetrieveMultiple(countychildquery);
                            if (countychild.Entities.Count() > 0)
                            {
                                for (int j = 0; j < 1; j++)
                                    CountyView.ChildType = countychild.Entities[j].Contains("ccon_type") ? countychild.Entities[j].FormattedValues["ccon_type"].ToString() : "null";
                            }

                            CountiesList.Add(CountyView);
                        }
                        if (CountiesList.Count > 0)
                        {
                            serviceResponse.ResponseType = "1";
                            serviceResponse.Data = CountiesList.OrderBy(x => x.CountyName);
                            serviceResponse.ResponseMessge = "Data Found Successfully";
                        }
                        else
                        {
                            serviceResponse.ResponseType = "2";
                            serviceResponse.Data = null;
                            serviceResponse.ResponseMessge = "Data Not Found";
                        }

                    }
                    else
                    {
                        serviceResponse.ResponseType = "2";
                        serviceResponse.Data = null;

                        serviceResponse.ResponseMessge = "Data Not Found";
                    }
                }
                catch (Exception ex)
                {
                    serviceResponse.ResponseType = "3";
                    serviceResponse.Data = null;
                    serviceResponse.ResponseMessge = ResponseMessage.RequestFailed;
                }

            }
            else
            {
                try
                {
                    EntityCollection city = new EntityCollection();
                    QueryExpression CityQuery = new QueryExpression()
                    {

                        EntityName = "ccon_mylocation",
                        ColumnSet = new ColumnSet(true),
                        Criteria = new FilterExpression
                        {
                            FilterOperator = LogicalOperator.And,
                            Conditions =
                        {
                            new ConditionExpression
                            {
                                AttributeName = "ccon_type",
                                Operator = ConditionOperator.Equal,
                                Values = { 757130004 }
                            },
                            new ConditionExpression
                            {
                                AttributeName = "ccon_parentlocation",
                                Operator = ConditionOperator.Equal,
                                Values = { locationHierarchyModel.GUID }
                            }
                        }
                        }
                    };
                    city = crmSvx.RetrieveMultiple(CityQuery);
                    if (city.Entities.Count() > 0)
                    {
                        for (int i = 0; i < city.Entities.Count(); i++)
                        {
                            cityview = new CityView();
                            cityview.CityGUID = city.Entities[i].Id.ToString();
                            cityview.CityName = city.Entities[i].Attributes["ccon_name"].ToString();
                            cityview.ParentLocation = city.Entities[i].Contains("ccon_parentlocation") ? city.Entities[i].GetAttributeValue<EntityReference>("ccon_parentlocation").Name.ToString() : "null";
                            cityview.ChildType = "null";

                            //provinceView.ChildType = provincechild.Entities[j].Contains("ccon_type") ? provincechild.Entities[j].FormattedValues["ccon_type"].ToString() : "null"; ;

                            citylist.Add(cityview);
                        }
                        if (citylist.Count > 0)
                        {
                            serviceResponse.ResponseType = "1";
                            serviceResponse.Data = citylist.OrderBy(x => x.CityName);
                            serviceResponse.ResponseMessge = "Data Found Successfully";
                        }
                        else
                        {
                            serviceResponse.ResponseType = "2";
                            serviceResponse.Data = null;
                            serviceResponse.ResponseMessge = "Data Not Found";
                        }

                    }
                    else
                    {
                        serviceResponse.ResponseType = "2";
                        serviceResponse.Data = null;
                        serviceResponse.ResponseMessge = "Data Not Found";
                    }
                }
                catch (Exception ex)
                {
                    serviceResponse.ResponseType = "3";
                    serviceResponse.Data = null;
                    serviceResponse.ResponseMessge = ResponseMessage.RequestFailed;
                }
            }


            return serviceResponse;
        }
       
    }
}
